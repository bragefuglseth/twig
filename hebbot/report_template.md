---
title: "#1 This Week in GNOME"
author: {{author}}
date: {{today}}
tags: [{{projects}}]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from {{timespan}}.<!--more-->

{{sections}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
