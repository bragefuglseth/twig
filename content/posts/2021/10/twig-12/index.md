---
title: "#12 Inspecting Libadwaita"
author: Felix
date: 2021-10-01
tags: ["blanket", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 24 to October 01.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita now [provides](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/251) a GtkInspector page extension for testing the system color schemes and high contrast mode. This can be useful for testing color schemes without a GNOME 42 jhbuild session or a GNOME OS VM. If you want to use the inspector page with a Flatpak app that bundles libadwaita, you'll have to specify `"--env=GTK_PATH=/app/lib/gtk-4.0"` in your `finish-args`. Nightly SDK already contains it, so it should work automatically there.
> ![](3e3339d7f261cb184e7470556564b2a3448cbcd2.png)

# Circle Apps and Libraries

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> This week [Text Pieces](https://apps.gnome.org/app/com.github.liferooter.textpieces/) entered GNOME Circle. Text Pieces allows you to apply several predefined and custom “tools” to your text. Examples for included tools are, JSON to YAML conversion, sort, search and replace, or Base64 decode, to only name a few. Congratulations!
> ![](02415da14e1ef0b3d2868470130777cde0e95022.png)

### Blanket [↗](https://github.com/rafaelmardojai/blanket)

Improve focus and increase your productivity by listening to different sounds.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Blanket 0.5.0 is out and available at [Flathub](https://flathub.org/apps/details/com.rafaelmardojai.Blanket). It comes with many improvements as the ability to create presets, toggling sounds more easily, a dark mode option and 13 new translations.

# Third Party Projects

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) reports

> Phosh (a graphical shell for mobile devices) now provides immediate feedback when launching an app by showing a launch splash screen:
> ![](WXwjQQJeuJCwvTkhjBDOosoM.png)
> ![](PsUISgUjiyxzbMUClDrphijO.png)

# GNOME Shell Extensions

[Simon Schneegans](https://matrix.to/#/@schneegans:matrix.org) reports

> [Fly-Pie](https://github.com/Schneegans/Fly-Pie), the marking menu extension for GNOME Shell, has been updated to support GNOME Shell 40+. You can use it to launch applications, simulate hotkeys, open URLs and much more by drawing gestures. The new version also includes a much more intuitive WYSIWYG menu editor! 
> 
> https://www.youtube.com/watch?v=sRT3O9-H5Xs

# Miscellaneous

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) announces

> Weather, Characters, and Disk Usage Analyzer all now support the new system-wide dark style preference.
> ![](12a4360c01c9d08fb02a75d5d97d4f3c261bf9c8.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
