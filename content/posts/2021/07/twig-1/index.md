---
title: "#1 Scrolling in the Dark"
author: Felix
date: 2021-07-16
tags: ["webkitgtk", "gnome-shell", "gtk-rs", "obfuscate", "libadwaita", "gnome-builder"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 09 to July 16.<!--more-->

# Core Apps and Libraries
### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> WebKitGTK now [uses](https://bugs.webkit.org/show_bug.cgi?id=227834) dark scrollbars for dark pages. This works even for pages that hadn't explicitly declared dark mode support.
![](webkit1.png)

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> WebKitGTK now [supports](https://bugs.webkit.org/show_bug.cgi?id=208204) the `color-scheme` CSS property, allowing to use dark form controls and system colors.
![](webkit2.png)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> [A patchset that helps reduce input latency on Wayland](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1762) has been merged in Mutter. Users should expect a slight decrease in input latency, especially on lower refresh-rate displays such as 60 Hz. In rare cases this patchset could also give a slight boost to the FPS in GNOME Shell.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> I finished corner drag-and-drop support for area selection and initial window selection implementation in the [new GNOME Shell screenshot UI](https://imolodetskikh.wordpress.com/2021/06/29/gsoc-2021-gnome-shell-screenshot-ui/). Find more details and demos in my [update blog-post](https://imolodetskikh.wordpress.com/2021/07/15/gsoc-2021-selection-editing-and-window-selection/)!
{{< video src="shell1.mp4" >}}

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> A [Cawbird](https://github.com/IBBoard/cawbird) developer has implemented [unread badges](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/165) for `AdwViewSwitcher` in libadwaita
![](adwaita1.png)

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[vanadiae](https://matrix.to/#/@vanadiae:matrix.org) reports

> This past week(s), I've done some work to improve Builder's session saving. One of the primary reason that made me start this work is to have the ability to restore all the different pages types that Builder has, as currently only the main source code pages were restored on startup. The current session API made plugins handle the restoring of both the pages and their position in the workspace grid, which meant that it was nearly impossible for multiple addins to restore their pages in the right position in a reliable and straight-forward way. So I [reworked the whole Session API](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/345) to make addins only handle the content of their pages, without requiring them to care about restoring to the grid. This allowed me to [add support for Devhelp documentation pages](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/395) much more easily. So now it should be easy to add support for other pages types, like terminal pages or the folder browser pages. I've also [fixed a small issue](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/398) which was already there before the rework, which made the visible pages not always being the visible ones when restoring, which can be annoying as you'd need to switch to them again from the Open Pages popover to resume your work. Builder is also on the road to gain support for [autosaving the session](https://gitlab.gnome.org/GNOME/gnome-builder/-/merge_requests/400) when a page is opened, moved or closed, or if you e.g. changed directory in a terminal, so that if Builder ever crashes you won't need to re-open all the pages you were working on, hence making crashes less disruptive. All those additions will make their way into GNOME 41, to be released in September.

# Circle Apps and Libraries
### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) reports

> The [gtk-rust-template](https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template) has been updated to use the latest release of gtk4-rs. Additionally, gtk3-rs support has been dropped and the template script has been simplified. (see [!45](https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template/-/merge_requests/45) and [!46](https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template/-/merge_requests/46))

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> gtk4-rs got a 0.2 release already. It includes API fixes for `gsk::RenderNode` / `gsk::Renderer` / `gtk::Snapshot` / `gdk::Drop` / `gtk::DropTargetAsync`

### Obfuscate [↗](https://gitlab.gnome.org/World/obfuscate)

Censor your private information on any image.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> Obfuscate got ported to GTK 4 in the latest release. The only noticeable difference by the end-user is very fast rendering as the application doesn't use cairo for that anymore.

# Third Party Projects
[Felix](https://matrix.to/#/@felix:haecker.io) announces

> I worked on [hebbot](https://github.com/haecker-felix/hebbot) which gets used to generate "This Week in GNOME" blog posts. You can read more about it in my [blog post](https://blogs.gnome.org/haeckerfelix/2021/07/16/introducing-this-week-in-gnome/)!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

