---
title: "#9 Headerbar Cleanup"
author: Felix
date: 2021-09-10
tags: ["libadwaita", "fractal", "gnome-software", "gtk-rs", "dejadup", "polari", "telegrand"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 03 to September 10.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> [libadwaita has simplified button appearance in header bars](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/233). Check [the corresponding blog post](https://blogs.gnome.org/alexm/2021/09/08/cleaning-up-header-bars/) for more details, and if you maintain an app using it, be sure to update it!
> ![](2f6d794d77268238ec22726928dba6a435b1c7ad.png)

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) reports

> All GNOME Circle apps [have been added](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/974) to Software’s default list of featured apps

# GNOME Releases

[alatiera](https://matrix.to/#/@alatiera:matrix.org) reports

> The GNOME 41 Release Candidate is out! https://discourse.gnome.org/t/gnome-41-rc-released/7500?u=alatiera

# Circle Apps and Libraries

### Déjà Dup Backups [↗](https://wiki.gnome.org/Apps/DejaDup/)

A simple backup tool.

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) reports

> [Déjà Dup](https://wiki.gnome.org/Apps/DejaDup) landed support for avoiding scheduled backups during Power Saver mode and GameMode. And redesigned its "oauth access granted" page to be much prettier and support dark mode:
> ![](89c23c11e868d7f20428b5d12699193b899a58b7.png)

### Polari [↗](https://wiki.gnome.org/Apps/Polari/)

IRC client which enables you to chat with people around world through large chatrooms or via private messaging.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week [Polari](https://apps.gnome.org/app/org.gnome.Polari/) entered [GNOME Circle](https://circle.gnome.org). Polari is the first Chat app to join the Circle. Congratulations!

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Aaron Erhardt](https://matrix.to/#/@admin:matrix.aaron-erhardt.de) announces

> The first stable version of Relm4, an idiomatic GUI library based on gtk4-rs, was released with the goal to make developing GTK4 apps in Rust simpler and more productive!
> 
> Most notably, Relm4 now offers support for libadwaita, a complete book for beginners and lots of other improvements. The full release article can be found [here](https://aaronerhardt.github.io/blog/posts/announcing_relm4/).
> ![](QgENLgTPpZeBQjMJszDPbPSC.png)

# Third Party Projects

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) says

> Exciting stuff happened in [Telegrand](https://github.com/melix99/telegrand) over this week! I implemented the day dividers in the chat history and I also added the sender of the last sent messages in the chat list. I also enabled the tdlib's message database feature that allows to use Telegrand in offline mode and that also speeds up the opening time.
> 
> Rodrigost23 colored the sender names using the same color scheme used in Telegram Desktop and also added an icon for the pinned chats.
> ![](7793ab5d1f2c1f9eb33a9c1262b16f10385697cf.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) announces

> Our biggest news is that **[multi-account support landed in fractal-next](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/774)** (don’t be fooled by the title of the MR, it’s more than just a widget 🥷). I feel like this is one of the most requested features across all clients, yet not many have it yet, and I’m extatic that we’re joining them 🎉. This work was done as part of GSoC by Alejandro under the mentorship of Julian 👏.
> 
> ![](dJDllSJBiEfcBkytZLlIJrPa.png)
> 
> Kai made it so that [rooms are sorted by activity](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/810) in fractal-next, like they already are in stable ✔️. He also [fixed module inception](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/823), for better code quality 🐛.
> 
> Julian landed a whole bunch of changes ❗️ He added [scrolling and a scroll to bottom button](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/825), [fixed keyboard shortcuts](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/831) and [a wrapping issue with long “words” that caused the timeline to jump to a wider size](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/829). But all this pales in comparison to [loading previous events](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/826) 🤯

# Miscellaneous

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) says

> The list of GNOME Core apps on [apps.gnome.org](https://apps.gnome.org) is finally complete. Because of some technical challenges, a hand full of apps were still missing. I have fixed or worked around all of those problems and we can finally enjoy the completed list.
> 
> There is also a [new feature](https://twitter.com/SophieInFoss/status/1435994672070078466) for people that like to chat. If available, the project's Matrix channel is now linked in the “Get involved” section.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

