---
title: "#11 Forty-one!"
author: Felix
date: 2021-09-24
tags: ["kooha", "metadata-cleaner", "libadwaita", "gtk-rs", "dejadup"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 17 to September 24.<!--more-->
In this week we have released GNOME 41.

![](41_banner.png)

This new major version of GNOME is full of exciting new features like a completely [redesigned Software app](https://thisweek.gnome.org/posts/2021/08/twig-7/#software), [new multitasking settings](https://thisweek.gnome.org/posts/2021/08/twig-6/#gnome-shell), [better power management](https://thisweek.gnome.org/posts/2021/07/twig-2/#settings) - and of course much more. More information can be found in the [GNOME 41 release notes](https://help.gnome.org/misc/release-notes/41.0/).

Readers who have been following this site for a few weeks will already know some of the new features. If you want to follow the development of GNOME 42 (March 2022), keep an eye on this page - we'll be posting exciting news every week!

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> libadwaita and libhandy now have [API](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.StyleManager.html) for interacting with the upcoming cross-desktop dark style preference. If you're maintaining an app using libadwaita, make sure it works with it
> {{< video src="009d8f90420a1ec9a8e4082b96a7c9b99453fd11.webm" >}}

# Circle Apps and Libraries

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) says

> This week [Commit](https://apps.gnome.org/app/re.sonny.Commit/) entered GNOME Circle. Commit helps you write better Git and Mercurial commit messages. Congratulations!
> ![](9597bb8c4bd15917a43bc189778f0a00e22961f6.png)

> As another addition to GNOME Circle, we welcome [Metronome](https://apps.gnome.org/app/com.adrienplazas.Metronome/) this week. Metronome does not only allow to keep the tempo, but it also serves as a beat counter. Congratulations!
> ![](da05e2517562210a4f314180b0cba561c8199b9a.png)

### Metadata Cleaner [↗](https://gitlab.com/rmnvgr/metadata-cleaner)

View and clean metadata in files.

[Romain](https://matrix.to/#/@romainvigier:matrix.org) says

> I released v2.0.0 of [Metadata Cleaner](https://metadatacleaner.romainvigier.fr)! This is a major upgrade featuring a brand new user interface built with GTK4 and libadwaita, a new help system and a whole set of new and updated translations.
> ![](YxGBJQdHMzBkpespiNncGHBO.png)

### Kooha [↗](https://github.com/SeaDve/Kooha)

A simple screen recorder with a minimal interface. You can simply click the record button without having to configure a bunch of settings.

[SeaDve](https://matrix.to/#/@sedve:matrix.org) reports

> Kooha version 2.0.0 is now available at [Flathub](https://flathub.org/apps/details/io.github.seadve.Kooha). It comes with many new bug fixes and features, such as GIF format and opt-in hardware accelerated encoding. For full changelog, see the [release page](https://github.com/SeaDve/Kooha/releases/tag/v2.0.0).

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> GTK 4.4 API changes landed in gtk4-rs in its latest release 0.3.

### Déjà Dup Backups [↗](https://wiki.gnome.org/Apps/DejaDup/)

A simple backup tool.

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) says

> Déjà Dup 43.alpha has been released! It adds support for Microsoft OneDrive, experimental opt-in support for the Restic backup tool, and a general UI refresh. See the [call-for-testing forum post](https://discourse.gnome.org/t/call-for-testing-deja-dup-43-alpha/7602) if any of that interests you.

# Third Party Projects

[Felix](https://matrix.to/#/@felix:haecker.io) reports

> I released [Audio Sharing](https://flathub.org/apps/details/de.haeckerfelix.AudioSharing)! It's a small tool which allows you to stream your current computer audio over the network as a RTSP stream. 
> 
> By sharing the audio as a network stream, you can use devices that are not intended to be used as audio sinks (eg. smartphones) to receive it. For example, there are audio accessories that are not compatible with desktop computers (e.g. because the computer does not have a Bluetooth module installed). With the help of this small tool, the computer audio can be played back on a smartphone, which is then connected to the Bluetooth accessory.
> ![](DyAmGQLHbFLrmwTyQDnrNatV.png)

[Forever](https://matrix.to/#/@foreverxmld:matrix.org) reports

> My randomization app, Random, has version 0.9 up on [Flathub](https://flathub.org/apps/details/page.codeberg.foreverxml.Random). This release brings working translations to 2 languages, along with the features from older releases.
> ![](cIoYpUHvlDgrnBCjEIxeLWAa.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
