---
title: "#17 Hourly Backups"
author: Felix
date: 2021-11-05
tags: ["metadata-cleaner", "fractal", "telegrand", "pika-backup"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 29 to November 05.<!--more-->

# Circle Apps and Libraries

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) says

> Basic support for hourly, daily, weekly, and monthly backups has been merged into Pika Backup. Accordingly, the overview of configured backups has been updated to show the schedule status for each configuration.
> ![](c50f8f0b6eede896a6bb5e9e22cf1cb1ef7bb3a2.png)

### Metadata Cleaner [↗](https://metadatacleaner.romainvigier.fr/)

View and clean metadata in files.

[Romain](https://matrix.to/#/@romainvigier:matrix.org) reports

> I released version 2.1.0 of Metadata Cleaner. It now allows adding whole folders at once and brings a few other improvements! [Read the full release notes](https://gitlab.com/rmnvgr/metadata-cleaner/-/releases/v2.1.0)

# Third Party Projects

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> Rhythmbox can now be built with built with Meson instead of Autotools: https://gitlab.gnome.org/GNOME/rhythmbox/-/merge_requests/86

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Toni Ruža implemented keyboard support for playing Hitori without a mouse (https://gitlab.gnome.org/GNOME/hitori/-/merge_requests/34)
> ![](KTTxmLWbqlDiXPcPxRDyAVcA.png)

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) says

> Telegrand got a new contributor: Marcus Behrendt. He's been doing a lot of awesome contributions. An handful of his work in the past few weeks:
> 
> * Implemented the [registration of a new Telegram account](https://github.com/melix99/telegrand/pull/89)
> * Implemented [login using QR Code](https://github.com/melix99/telegrand/pull/114)
> * Implemented the [password recovery and reset account functionality](https://github.com/melix99/telegrand/pull/104)
> * Added the [previews of new message types in the sidebar](https://github.com/melix99/telegrand/pull/87)
> * Various cleanups and performance improvements
> 
> On the other hand, I have implemented the [user info dialog](https://github.com/melix99/telegrand/pull/118) and the [reporting of the chat typing actions](https://github.com/melix99/telegrand/pull/98). I've also made some general [style improvements](https://github.com/melix99/telegrand/pull/122) and [RTL fixes](https://github.com/melix99/telegrand/pull/125).
> ![](cce82e5e87b3580e3c406c59bcbb6370d9141bf9.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) says

> To adapt to changes in libadwaita, Marco Melorio [removed a deprecated class](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/864). In the meantime, enterprisey [fixed the build by updating the Matrix Rust SDK dependency](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/866) and [improved Secret Service error handling](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/844).
> 
> After being away for a few month, Kévin Commaille came back with a blast. No less than three MR already, and I have a feeling he’s only getting started:
> 
> * [pretty printing for event source](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/869)
> * [crash prevention on login failures](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/868)
> * [several fixes for text messages](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/870)
> 
> As usual, Julian reviewed and merged all the above work, and did some of his own:
> 
> * [fixed a crash on room history loading](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/865)
> * [use crossigning to verify new sessions](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/854)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

