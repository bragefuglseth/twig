---
title: "#37 Absolutely Serious"
author: Felix
date: 2022-04-01
tags: ["just-perfection", "shortwave", "pika-backup", "webfont-kit-generator", "webkitgtk", "fractal", "identity"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 25 to April 01.<!--more-->

# Core Apps and Libraries

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) says

> GNOME Logs was [ported to GTK 4](https://gitlab.gnome.org/GNOME/gnome-logs/-/merge_requests/30) and libadwaita 🎊.
> ![](e8c9f3bf82ba3e657876e7f46be9d4ee965494ec.png)

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> WebKitGTK Adwaita widgets, as well as scrollbars, now more closely resemble their libadwaita versions than GTK3 versions, and support CSS [accent-color](https://developer.mozilla.org/en-US/docs/Web/CSS/accent-color) following the Cocoa WebKit ports
> ![](79adf0cecbd38c0c341b4374d9fc75c9ab4edbc8.png)

# Circle Apps and Libraries



### Webfont Kit Generator [↗](https://github.com/rafaelmardojai/webfont-kit-generator)

Create @font-face kits easily.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Webfont Kit Generator 1.0.0 is out and available in [Flathub](https://flathub.org/apps/details/com.rafaelmardojai.WebfontKitGenerator)!
> 
> Major changes are:
> * Ported to GTK4 and libadwaita
> * New Google Fonts importer
> * New app icon by Tobias Bernard
> 
> The app now will be in maintenance mode.

### Shortwave [↗](https://gitlab.gnome.org/World/Shortwave)

Internet radio player with over 25000 stations.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> It is now possible to add [local stations](https://gitlab.gnome.org/World/Shortwave/-/merge_requests/434) in Shortwave that are not publicly published on radio-browser.info. I also added a new transition for the miniplayer mode using [Adw.TimedAnimation](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.TimedAnimation.html).
> ![](CDVRnQUUgVNAokblJjvhyzEu.png)
> {{< video src="BazSUgnFKkDobpiewtcFiPso.webm" >}}

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) reports

> A new release for Pika Backup is planned for May the 15th. This means that version 0.4 will be released exactly one year after version 0.3.
> 
> This week I landed the following:
> 
> * Added desktop notifications for postponed scheduled backups. Backups can be postponed if the repository is in use, the internet connection is metered, or the device is not connected to power.
> 
> * Updated some text and icons in the user interface.
> 
> * Added a desktop notification for crashes of the app running the backups. This should only happen on unlikely events like segfaults in underlying libraries or saturated memory.
> 
> * Added archives prefixes to the user interface including a dialog to change them.
> * Until now, BorgBackup created checkpoints every 30 minutes in Pika Backup. Checkpoints are points in the backup process from where you can continue an incomplete backup.
> 
>   Since BorgBackup 1.2 it is also possible to create a checkpoint when manually aborting a running backup. Now, this is also the default when aborting a backup in Pika Backup.
> * A lot of technical details around the background monitor that enables scheduled backups have been fixed.
> * The dialog that asked for the repository password now shows which repository is requiring the password.
> 
> * Added support for *Fnmatch* (shell wildcard patterns) to the backend. Adding them outside the config file will probably be delayed until version 0.5.
> * Continued to make the setup workflow more pleasing.
> * Fixed several bugs in showing when the next backup is scheduled. 
> * Notify about connected backup devices if they aren't set up for scheduled backups, to quickly start a backup from the desktop notification.
> * Properly notify about missing devices for backup that is scheduled for this moment.
> ![](1b810d484cfe5bafe7f5e645c3bc4c6306900b92.png)

### Identity [↗](https://gitlab.gnome.org/YaLTeR/identity)

Compare images and videos

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) reports

> Identity v0.3 is out with zoom support! You can set the scale to 100% for pixel-perfect comparisons, or zoom in and out with mouse, touchpad or touchscreen. Zoom and view position are synchronized across open files.
> {{< video src="6a20927d0e554aecc03584c29cb2e72784ae1b55.webm" >}}

# Third Party Projects

[rickykresslein](https://matrix.to/#/@rickykresslein:matrix.org) says

> [Furtherance](https://github.com/lakoliu/Furtherance) is a new time tracking app written in Rust using GTK 4 and libadwaita. 
> Features include:
> * Track time spent on individual tasks
> * Idle detection on GNOME
> * Task names and times can be edited
> * Tasks are sorted by day and similar tasks are grouped
> ![](BCZWCYsLMxLGIzqkpGqJCmDd.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Julian Sparber](https://matrix.to/#/@jsparber:gnome.org) says

> This week I have something really exciting to report. We moved Fractal-next to the main development branch. This means that we now have nightly flatpak builds\[1\], so users can already try out the new version of Fractal without having to build it from source. This will also give use much more feedback and many more bug reports, hopefully not too many :) Note that this isn't a release and the software should still be considered  unstable.
> 
> \[1\] https://gitlab.gnome.org/GNOME/fractal#development-version

# GNOME Shell Extensions

[firox263](https://matrix.to/#/@firox263:matrix.org) reports

> [Extension Manager](https://github.com/mjakeman/extension-manager/), a simple app for browsing and installing Shell Extensions, had its third release. This update adds some of the final missing pieces when  compared with the extensions website.
> 
> The highlights are:
>  - Displays comments and reviews
>  - Support for updating in-app
>  - A new GNOME-style app icon
>  - Improved handling of errors
>  - Correct labelling of out-of-date extensions
>  - Significantly reduced file size
> ![](YJYAdKgodrAzawjLFfywnAET.png)

### Just Perfection [↗](https://extensions.gnome.org/extension/3843/just-perfection/)

A tweak tool to customize the GNOME Shell and to disable UI elements.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) reports

> [Just Perfection extension](https://extensions.gnome.org/extension/3843/just-perfection/) version 20 has been released with some bug fixes and two new features (calendar and events visibility).
> This version is named after Francesco Hayez (Italian painter).
> ![](bdb32a0e4df4a1d783df171f049ecaaaecb9bb66.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

