---
title: "#40 Rows and Containers"
author: Thib
date: 2022-04-22
tags: ["amberol", "pods", "authenticator", "gtk-rs", "furtherance", "pika-backup", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 15 to April 22.<!--more-->

# Core Apps and Libraries

### Sushi [↗](https://gitlab.gnome.org/GNOME/sushi)

A file previewer for the GNOME desktop environment.

[feborges](https://matrix.to/#/@felipeborges:gnome.org) announces

> I am looking for a new maintainer to take care of Sushi (also known as the NautilusPreviewer, a quick previewer for GNOME Files).  I will be happy to help a new maintainer onboard and make releases. For more information, visit https://discourse.gnome.org/t/looking-for-a-new-maintainer-for-sushi/9361

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> I've finished and landed entry rows originally [implemented](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/194) by Maximiliano. So now we have [`AdwEntryRow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.EntryRow.html) and [`AdwPasswordEntryRow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.PasswordEntryRow.html)
> {{< video src="b0f437fab1103d285ff0cb2dfd357a3b78c99c82.mp4" >}}

# Circle Apps and Libraries

### Pika Backup [↗](https://wiki.gnome.org/Apps/PikaBackup)

Simple backups based on borg.

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) announces

> When setting up an existing backup repository, Pika Backup now offers the option to infer settings from existing archives in the repository. If you previously used BorgBackup with a different tool or via the command line, this can help you to configure Pika Backup.
> 
> To optimize performance, new repositories are now initialized with the, in this case, faster BLAKE2 hash algorithm, if the current system does not support SHA256 CPU instructions.
> ![](f39258b7005541d48b2b31dda9d0360ea20818ef.png)

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian Hofer](https://matrix.to/#/@julianhofer:gnome.org) reports

> I've added the (fast) [`mold`](https://github.com/rui314/mold) linker to the [`org.freedesktop.Sdk.Extension.rust-stable`](https://github.com/flathub/org.freedesktop.Sdk.Extension.rust-stable) extension.
> This way, Rust based projects using flatpak can easily benefit from decreased build times.
> The time spent on linking is especially noticeable for incremental builds.
> For example, the incremental build of Fragments now only takes 4 instead of 12 seconds.
> The corresponding MR can be found [here](https://gitlab.gnome.org/World/Fragments/-/merge_requests/136/diffs).
> 
> Instructions:
> 
> 1. Add the `org.freedesktop.Sdk.Extension.llvm13` extension along with this extension in order to get `clang`.
> 2. Add `/usr/lib/sdk/llvm13/bin` to `append-path`.
> 3. Set environment variables:
> 
>     - `CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER` to `clang`, and
>     - `CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUSTFLAGS` to `-C link-arg=-fuse-ld=/usr/lib/sdk/rust-stable/bin/mold`.
> 
> In total, the changed parts of your flatpak manifest should look like this:
> 
> ```json
> {
>     "sdk-extensions": [
>         "org.freedesktop.Sdk.Extension.rust-stable",
>         "org.freedesktop.Sdk.Extension.llvm13"
>     ],
>     "build-options": {
>         "append-path": "/usr/lib/sdk/rust-stable/bin:/usr/lib/sdk/llvm13/bin",
>         "env": {
>             "CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER": "clang",
>             "CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_RUSTFLAGS": "-C link-arg=-fuse-ld=/usr/lib/sdk/rust-stable/bin/mold",
>             "CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_LINKER": "clang",
>             "CARGO_TARGET_AARCH64_UNKNOWN_LINUX_GNU_RUSTFLAGS": "-C link-arg=-fuse-ld=/usr/lib/sdk/rust-stable/bin/mold"
>         }
>     }
> }
> ```

### Authenticator [↗](https://gitlab.gnome.org/World/Authenticator)

Simple application for generating Two-Factor Authentication Codes.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> I have finally took the time to make a new release of [Authenticator](https://beta.flathub.org/apps/details/com.belmoussaoui.Authenticator) including various fancy features. Here is a quick summary of what we have been working on:
> 
> * Port to GTK 4
> * Encrypted backup support
> * Use the camera portal for scanning QR codes
> * GNOME Shell search provider
> * Allow editing accounts provider
> * Better favicons detection
> * Refined user interface

# Third Party Projects

### Pods [↗](https://github.com/marhkb/pods)

A podman desktop application

[marhkb](https://matrix.to/#/@marcusb86:matrix.org) announces

> A lot has happened since the initial announcement of Symphony, a desktop application for Podman.
> 
> First, Symphony was renamed to [Pods](https://github.com/marhkb/pods). Thanks to GitHub user [fostertheweb](https://github.com/marhkb/pods/issues/99#issuecomment-1080088366) for this suggestion. The name is more concise and avoids associations with a music application.
> 
> Other new features include:
> 
> * A manual dark mode, which can be activated independently of the system style
> * Image details are now displayed in a separate page within a leaflet instead of in an ExpanderRow
> * You can now open a dialog to display basic information about Podman
> * Containers can now be renamed easily via a dialog
> * The prune dialog has been reworked and now offers more options
> * A circular indicator now gives information about a container's CPU and memory status
> * Container logs can now be viewed and searched
> * A dialog can now be used to create and start new containers from existing images
> ![](PrdZTLLAbiBHoaldzzgZNZWD.png)
> ![](AmVcGSwWfYFOGSfVzylrQpbb.png)
> ![](LbAxHFdjqiBzbMlFodsDGpHT.png)
> ![](HPHpttbqUHrhfVKSSllHaLYI.png)
> ![](FwbIhWrhmzdcWxGIcRSSOAoc.png)

### Furtherance [↗](https://github.com/lakoliu/Furtherance)

Track your time without being tracked

[rickykresslein](https://matrix.to/#/@rickykresslein:matrix.org) says

> [Furtherance](https://github.com/lakoliu/Furtherance), a time-tracking app for GNOME, version 1.1.2 was released with the ability to add tags! Some smaller improvements: the app icon is now aligned better, the start and delete buttons are now blue and red, respectively, and three more translations were added.
> ![](WCAftxwNEEBkAVXhVpcEkYOx.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> It's Friday, and there's a new release of Amberol, the music player with no delusions of grandeur! In this release I've added a few niceties, like being able to see the position in waveform of the currently playing song; a remove button for modifying the playlist; and a whole new adaptive UI layout that works just as well on the desktop as it does on a mobile device. You can find Amberol 0.4.0 on [Flathub](https://flathub.org/apps/details/io.bassi.Amberol).
> ![](a57c9618e244ec44c98d9d03004110d6c49c6d2f.png)
> ![](b1128c57617332fc234d3c08598a3692647f05ff.png)
> ![](b0252adc7ef9cf6aa846e1e7ff9f68a666e29989.png)

# GNOME Foundation

[Thib](https://matrix.to/#/@thib:ergaster.org) says

> Where is the Foundation even going? Not in the cloud!
> 
> I wrote this post to shed some light on a programme the Foundation wants to conduct, how it will impact the GNOME Project, and how the contributors can help shaping it.
> 
> https://discourse.gnome.org/t/foundation-strategy-funding-decentralised-local-first-applications/

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) says

> Registration is now open for GUADEC (July 20th - 25th) https://events.gnome.org/event/77/

[Neil McGovern](https://matrix.to/#/@nmcgovern:matrix.org) reports

> Linux App Summit (https://linuxappsummit.org/) starts on Friday 29th, check out the schedule at https://conf.linuxappsummit.org/event/4/timetable/#all

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
