---
title: "#46 Going Mobile"
author: Felix
date: 2022-06-03
tags: ["webkitgtk", "flatseal", "authenticator", "glib", "gnome-software", "gnome-shell", "gaphor", "amberol", "gnome-calls"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 27 to June 03.<!--more-->

# Core Apps and Libraries

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[verdre](https://matrix.to/#/@verdre:gnome.org) says

> News from the Shell team! We're improving the experience on small screens and things are progressing quickly, GNOME Shell might run on your phone sooner than you think. Check out our [blogpost](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/) for more information.
> ![](5eb93e767927cdfe9f7b58bf53cfc0ba0083c7fb.png)

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[adrian](https://matrix.to/#/@aperez:igalia.com) announces

> The recent WebKitGTK [2.36.3](https://webkitgtk.org/2022/05/28/webkitgtk2.36.3-released.html) release included fixes for a [number of security issues](https://webkitgtk.org/security/WSA-2022-0005.html) that allowed remote code execution. While we are not aware of any of them being exploited, it is nevertheless strongly recommended to update to the latest release.
> 
> There are a number of improvements in the multimedia backend as well: GStreamer elements known to be problematic are now explicitly by default, we have enabled capture from devices which can use hardware-accelerated encoding, fixed display capture using Pipewire, and improved streaming video playback.

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Milan Crha has added support for listing other apps by the same author in gnome-software
> ![](jXCHOAssZrTOpnJCQIOysaff.png)

### Calls [↗](https://gitlab.gnome.org/GNOME/calls)

A phone dialer and call handler.

[Evangelos](https://matrix.to/#/@evangelos.tzaras:talk.puri.sm) reports

> Calls can now do encrypted [VoIP](https://en.wikipedia.org/wiki/Voice_over_IP) calls with [SIP](https://en.wikipedia.org/wiki/Session_Initiation_Protocol) using [SRTP](https://en.wikipedia.org/wiki/Secure_Real-time_Transport_Protocol) 🎉
> Supporting SRTP roughly consisted of two parts:
> * Change the [GStreamer](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/540) [pipeline](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/541) to do SRTP instead of plain RTP
> * Handle the [key exchange](https://datatracker.ietf.org/doc/html/rfc4568) in the [signalling](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/545)
> 
> Please note that we don't make use of the encryption indicator yet, for more details see [this issue](https://gitlab.gnome.org/GNOME/calls/-/issues/446) where you can also find some [nice designs](https://gitlab.gnome.org/GNOME/calls/-/issues/446#note_1463379) by Sam Hewitt of how things should eventually behave.
> ![](vnJcSQLCyJpWSjPSkdJoxnRU.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Benjamin Berg has [fixed a nasty deadlock](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2561) in `GFileMonitor` in GLib

# Circle Apps and Libraries

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) says

> Gaphor [2.10.0](https://github.com/gaphor/gaphor/releases/tag/2.10.0) has been release last week. Among the improvements Activity diagrams have been extended. Model loading has been improved and Gaphor finally has full drag and drop support from the tree view to a diagram.
> ![](jXgEOvVmvxvnuCLdSclJnpJJ.png)

### Authenticator [↗](https://gitlab.gnome.org/World/Authenticator)

Simple application for generating Two-Factor Authentication Codes.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> A new bugfixes release of [Authenticator](https://flathub.org/apps/details/com.belmoussaoui.Authenticator) is out. The new release also migrates your tokens from the host keyring to inside the sandbox so other sandboxed apps can't access them.

# Third Party Projects

### Flatseal [↗](https://github.com/tchx84/Flatseal)

A graphical utility to review and modify permissions of Flatpak applications.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) says

> I am happy to [announce](https://blogs.gnome.org/tchx84/2022/05/31/flatseal-1-8-0/) the release of Flatseal 1.8.0 🎉. This new release comes with the ability to review and modify global overrides, highlight changes made by users, follow system-level color styles, support for more languages and a few bugs fixes.
> ![](UxrVfHuieFlvBzDXzEnPFYJt.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> New Amberol release! Lots of small UI fixes to improve consistency and give better feedback when importing songs in the playlist. Colors and spacing between elements have also been improved, as well as general reliability.
> ![](451263160ba3dd513dad2fc708c75a00e6cbef17.png)
> ![](96c35365a7f6011b0a0c197a0d5cdf1c2c7569d6.png)

# Miscellaneous

[federico](https://matrix.to/#/@federicomena:matrix.org) announces

> The accessibility infrastructure modules are now merged: https://viruta.org/accessibility-repos-are-now-merged.html

[ranfdev](https://matrix.to/#/@ranfdev:matrix.org) announces

> Frustrated by the standard .ui file writing experience (that xml is very verbose) and inspired by the [blueprint compiler](https://gitlab.gnome.org/jwestman/blueprint-compiler), I've decided to write a custom Domain Specific Language to generate .ui files.  Compared to blueprint compiler, this let's you use a complete programming language, with variables and functions, to generate your ui. Check out the [project page](https://github.com/ranfdev/nickel-gtk-ui) for more information
> {{< video src="RYskhVLjUGfOAEgqQZPVrdiC.mp4" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
