---
title: "#54 More Portings"
author: Felix
date: 2022-07-29
tags: ["gnome-podcasts", "webkitgtk", "kgx", "bottles", "rnote", "cawbird", "gnome-builder"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 22 to July 29.<!--more-->

# Core Apps and Libraries

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> GNOME Initial Setup has been ported to GTK4 and libadwaita
> ![](85fc787ad4bccc81ab27d8dcc854ba6d0fc98d96.png)
> ![](f68b704a8bb1224c6f0bdef47b314929c3112062.png)


### GNOME Console [↗](https://gitlab.gnome.org/GNOME/console)

A simple user-friendly terminal emulator.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> Console has been ported to GTK4
> ![](4887eda8dd8dc9746ee855b36450d8d7e3ad94f9.png)

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[adrian](https://matrix.to/#/@aperez:igalia.com) reports

> A new stable release of WebKitGTK is now available. Version [2.36.5](https://webkitgtk.org/2022/07/28/webkitgtk2.36.5-released.html) not only includes fixes for security issues, but also makes video playback work again in Yelp and fixes the `WebKitWebView::context-menu` signal in GTK4 builds. The first development release in the next series, [2.37.1](https://webkitgtk.org/2022/07/12/webkitgtk2.37.1-released.html), has been available for a couple of weeks, featuring the new WebRTC implementation based on GstWebRTC among many other improvements.

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> GNOME Builder has received more than a hundred commits since last update, and is quickly progressing towards feature parity after landing the massive port to GTK4. The highlights of the past weeks are:
> 
>  * In-file and project-wise searches are back
>  * Large refactoring of how project and global settings are layered
>  * Auto-hiding minimap
>  * XML and C indenters are back
>  * Introduction of a new action muxer and an alternative way to activate actions
>  * A variety of internal rearchitecturing in preparation for future changes
> ![](34892f142dd628fc8537f05b08b8d548205f8378.png)

# Circle Apps and Libraries

### Podcasts [↗](https://gitlab.gnome.org/World/podcasts)

Podcast app for GNOME.

[Jordan Petridis](https://matrix.to/#/@alatiera:matrix.org) says

> After more than a year of development, the GTK 4 port of Podcasts has been merged. Huge thanks to Christopher Davis and Maximiliano for the joined effort of porting the codebase, but as well to Bilal Elmoussaoui and Julian Hofer for the exhaustive reviews.
> 
> We are putting the finishing touches still but you will be able to enjoy a new release o Podcasts in the month ahead.
> ![](HkXHiZNnEzCSswnBIxewqwsZ.png)
> ![](QNbCsUDvyuRQDFABapuIiIEz.png)

# Third Party Projects

[Aaron Erhardt](https://matrix.to/#/@aaron:matrix.aaron-erhardt.de) announces

> The first beta of [Relm4](https://github.com/Relm4/Relm4) 0.5, an idiomatic GUI library based on gtk4-rs, was released with many improvements.
> 
> With this release, large parts of Relm4 were redesigned to be much more flexible and easier to use.
> Notable changes include better interoperability with gtk4-rs and many improvements to the view macro syntax which allows you to even use if-else expressions in the UI declaration! You can find more information about the release in the [official blog post](https://relm4.org/blog/posts/announcing_relm4_v0.5_beta).
> {{< video src="UAZP3F6UxTZKvSoErBGsVDRx3mrLD9o7.webm" >}}

### Rnote [↗](https://github.com/flxzt/rnote)

Sketch and take handwritten notes.

[flxzt](https://matrix.to/#/@flxzt:matrix.org) says

> [Rnote](https://github.com/flxzt/rnote), the freehand note-taking and sketching app received a new update: v0.5.4! The app now has a new icon and symbolic (with the help of @bertob, thanks! ), finally added text input (with typewriter sounds), a new PDF import dialog with the added option for different PDF spacing preferences. Screenshots can now be pasted directly from the clipboard (thanks @RickStanley ) and there is now the possibility to enable input constraints when creating shapes (thanks to @sei0o ).
> 
> More new features: two new selector modes ( selecting individually, selecting by intersecting with a drawn path ). The workspace browser got a much needed redesign, and now has customizable workspaces (inspired by the Paper app ). Finally the Marker pen style now draws underneath other strokes, making it possible to mark text without obstructing it.
> ![](XwHrTEkeevImEQccGhBeQYAk.png)
> ![](LpeIvYtavTIlaHigyHqupyvR.png)

### Cawbird [↗](https://ibboard.co.uk/cawbird/)

A native Twitter client for your Linux desktop.

[CodedOre](https://matrix.to/#/@coded_ore:matrix.org) says

> After a longer pause, I continue to work on the libadwaita rewrite of Cawbird.
> This week the following was added:
> * Support for Video and GIF playback
> * Using an redirect to automatically get the authentication code after authorization on the servers website.
> 
> We also now have automated Flatpak builds of the current development status, which you can get at https://github.com/CodedOre/NewCaw/actions
> ![](AfqPbRRFOuwbhSTrwhETFTEk.png)
> {{< video src="rsIrOIWYigPwDWEvqDbrsWiJ.mp4" >}}

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> Bottles 2022.7.28 was released!
> 
> We are introducing a new [versioning system](https://youtu.be/astxMzyxk0E) that reliably lets you downgrade to previous states in case a recent change goes wrong.
> 
> Additionally, we have implemented covers in Library Mode to automatically add cover images in games. We can't thank [SteamGridDB](https://www.steamgriddb.com/) enough for their amazing service and collaboration.
> 
> For more information about the new update, check out our [release page](https://usebottles.com/blog/release-2022.7.28/)!
> ![](fa388f8df4b5c805f437622dc3bb75122983953b.png)

# GNOME Shell Extensions

[lupantano](https://matrix.to/#/@lupantano:matrix.org) announces

> [ReadingStrip](https://github.com/lupantano/readingstrip) is a extension for Gnome-Shell with an equivalent function to a reading guide on the computer, that's really useful for people with dyslexia.
> 
> Major update: Vertical strip added. This is really useful for graphic designers who want to check if their margins and indentations line up properly when displayed on the screen.
> ![](dDoIOwruogCGXmwAxzOKDquU.gif)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

