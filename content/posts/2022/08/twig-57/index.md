---
title: "#57 Flashy Colors"
author: Chris 🌱️
date: 2022-08-19
tags: ["kooha", "gtk-rs", "newsflash", "bottles"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 12 to August 19.<!--more-->

# Circle Apps and Libraries



### NewsFlash feed reader [↗](https://gitlab.com/news-flash/news_flash_gtk)

Follow your favorite blogs & news sites.

[Jan Lukas](https://matrix.to/#/@jangernert:matrix.org) says

> Thanks to a contribution of Pierre Auclair NewsFlash gained the ability to render Latex math formulas inside of articles with the help of mathjax.
> ![NewsFlash showing an article with math formulas](RPYeAIfRBNZeBkoBpHAqrCpS.png)

### Kooha [↗](https://github.com/SeaDve/Kooha)

A simple screen recorder with a minimal interface. You can simply click the record button without having to configure a bunch of settings.

[SeaDve](https://matrix.to/#/@sedve:matrix.org) reports

> [Kooha](https://github.com/SeaDve) 2.1.0 was released with the following new features and fixes:
> 
> * Previously selected video sources are now remembered.
> * It is now possible to cancel while flushing the recording.
> * Settings toggle button now uses different icons to discern state more clearly.
> * Added 3 seconds delay option.
> * MP4 encoder failing to initialize on uneven resolutions is now fixed.
> * Fixed time display on time greater than an hour.
> * Recordings are now stored default in a "Kooha" XDG videos subfolder. (This won't affect existing settings.)
> * "Show in Files" button now highlights the recording in the file manager.
> * Improve support information through the about window.
> * Other error handling and stability improvements.

# Third Party Projects

[0xMRTT](https://matrix.to/#/@0xmrtt:matrix.org) announces

> This week, [Gradience](https://github.com/GradienceTeam/Gradience) (Adwaita Manager) have some improvements
> * Project was renamed to "[Gradience](https://github.com/GradienceTeam/Gradience)" to remove any confusion with official GNOME app. 
> * Monet [engine](https://github.com/avanishsubbiah/material-color-utilities-python) performance improved (2 sec in v0.2.0 and 2.5 minutes in v0.1.0)
> * Fixed invisible text in "cards" when Monet palette was applied
> * Small UI improvements
> ![Gradience showing the monet engine colors in light mode](DpDsuOEemkfJsUHfPBEMJBED.png)
> ![Gradience showing the monet engine colors in dark mode](EtHjVtiEWIJexLKcTzrSuXlE.png)

[Paulo](https://matrix.to/#/@raggesilver:matrix.org) says

> It's been over a month since [Black Box](https://flathub.org/apps/details/com.raggesilver.BlackBox) was announced on TWIG. Since then, a lot has changed. Most recently, version 0.12.0 brought:
> 
> * Support for searching text in the terminal
> * Support for customizing the number of lines kept in the buffer
> * Support for reserving part of the header bar to drag the window
> * Improved theme integration and refined UI
> * Lower CPU usage, thanks to an update in VTE
> 
> Also, 18 bugs have been fixed, and new translations have been added for 2 languages, with 2 more on the way. [Check it out!](https://flathub.org/apps/details/com.raggesilver.BlackBox)
> ![BlackBox terminal showing system information and a color palette](nLgMcyJsohsSTHGtVMfEUMYi.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) announces

> Bottles 2022.8.14 was released!
> 
> We are introducing [vmtouch](https://github.com/hoytech/vmtouch) to cache data and improve performance! This can be enabled from preferences.
> 
> We are also introducing a new dialog to configure [vkBasalt](https://github.com/DadSchoorse/vkBasalt) settings, to change effects and color lookup table!
> 
> To further integrate vkBasalt, we now allow modifying vkBasalt configurations per-bottle. This means that each bottle can now have a separate vkBasalt configuration!
> 
> We brought back the dark mode switcher as well, as many desktop environments and distributions don't come with a dark style preference. That way, users have the option to force dark mode.
> 
> Other changes include:
> 
> * Prevent programs from launching when clicking on the entry instead of the play button
> * Windows can now be closed by pressing Escape
> * The Library view now supports program settings overrides
> * “Add to Steam” and “Add Desktop Entry” now supports integrations (Epic, Ubisoft, etc.)
> * The bottles-cli “programs” option now list programs from integrations as well
> * Gamescope is now compatible with FSR
> * Minor UI improvements
> * Fix missing translations, thanks to @A6GibKm
> * Fix a bug in “Add to Steam”, was not supporting ~/.steam
> * Fix a regression on renaming programs, this was causing duplicated entries
> * Fix a bug in the internal-sandbox, wine symlinks were not unlinked if the username had special characters
> * Fix a bug in generating desktop entries for programs with spaces in their names
> * Fix a bug in the copy\_dll dependency action, wildcards were not handled correctly, thanks to @siroccal
> * Fix a bug in loading gstreamer libraries, were not respecting the bottle architecture
> * Fix a regression in library mode, adding a new program was causing a loop when the bottle name and path mismatched
> * Improved translations
> 
> For more information about the new update, check out our [release page](https://usebottles.com/blog/release-2022.8.14/)!
> ![Bottles vkBasalt settings](0f1cb2135830561db34064063701963ae8b0e05d.png)
> ![Bottles vmtouch settings](894af95c36d0354b4cd5870de17ea1741cec6e87.png)

# Documentation



### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[Julian 🍃](https://matrix.to/#/@julianhofer:gnome.org) says

> I've published two new sections of the gtk-rs book. They introduce Libadwaita, show how to add it to a gtk-rs app and let the Todo app use Libadwaita with minimal changes to the code base. You can find the libadwaita chapter here: https://gtk-rs.org/gtk4-rs/stable/latest/book/libadwaita.html. 
> 
> The sections have been reviewed by Alexander Mikhaylenko and Ivan Molodetskikh.

# Miscellaneous

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> 25 years ago this week, Miguel de Icaza [announced](https://mail.gnome.org/archives/gtk-list/1997-August/msg00123.html) GNOME, a project for creating a free software desktop environment and application development platform. A quarter of a century later, and the GNOME project is still here, working to make a better, free, open desktop for everyone. [Happy birthday, GNOME](https://happybirthdaygnome.org/), and let's raise a glass to the next 25 years!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
