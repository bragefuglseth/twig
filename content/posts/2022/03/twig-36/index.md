---
title: "#36 Forty-two!"
author: Felix
date: 2022-03-25
tags: ["phosh", "video-trimmer", "commit", "metadata-cleaner", "fractal", "apostrophe", "libadwaita", "portfolio"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 18 to March 25.<!--more-->

**This week we released GNOME 42!**

![](42_banner.png)

Is 42 the answer [to everything](https://en.wikipedia.org/wiki/42_(number))? Who knows, in any case GNOME now has an answer to the infamous dark mode question:

{{< video src="darkmode.webm" >}}

Another question that has now been answered: How to make screenshots and screencasts easily? - Just press `Print Screen` on your keyboard:

{{< video src="screenshots.webm" >}}

GNOME 42 also includes other notable new features. Several apps have been ported to GTK4 / libadwaita, there is a new text editor, a new console app, and much more. More information can be found in the release notes.

- [Release announcement](https://foundation.gnome.org/2022/03/23/introducing-gnome-42/)
- [Release notes](https://release.gnome.org/42/)
- [What's new for developers](https://release.gnome.org/42/developers/)
- [Release video](https://www.youtube.com/watch?v=du-2QpWbiLU&feature=emb_title)

Readers who have been following this site for a few weeks will already know some of the new features. If you want to follow the development of GNOME 43 (later this year 2022), keep an eye on this page - we'll be posting exciting news every week!


# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) announces

> I've released libadwaita 1.1 and libhandy 1.6: https://blogs.gnome.org/alexm/2022/03/19/libadwaita-1-1-libhandy-1-6/

# Circle Apps and Libraries

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> Identity v0.2, ported to GTK 4 and libadwaita, is out, just in time for the GNOME 42 release! The old file switcher has been replaced with tabs which you can rearrange and drag between windows.
> 
> Video Trimmer also got updated with new translations.
> ![](6f857908c542ff7a5e08a4f199ef8f8b85c40f57.png)

### Metadata Cleaner [↗](https://metadatacleaner.romainvigier.fr/)

View and clean metadata in files.

[Romain](https://matrix.to/#/@romainvigier:matrix.org) announces

> I released version 2.2 of [Metadata Cleaner](https://metadatacleaner.romainvigier.fr), my app for viewing and removing metadata from files. It now uses the GNOME 42 runtime, features some user interface improvements and fixes a few bugs!
> ![](RhvTAVyJrKwkgRDFJeKVvDzG.png)

### Commit [↗](https://github.com/sonnyp/Commit)

An editor that helps you write better Git and Mercurial commit messages.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> [Commit message editor](https://apps.gnome.org/app/re.sonny.Commit/) version 3.1.0 is out
> 
> * Now powered by GtkSourceView
> * Highlight syntax for Git, Mercurial, and diffs
> * New keyboard shortcuts, see Ctrl+?
> * Automatically capitalize the commit title
> * Smart body wrapping
> * Various fixes
> ![](srNhsvMDCqzHDhcDzDCukjNk.png)

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) reports

> I've released Apostrophe 2.6, which among other things features multiwindow support, dark style preference support, and an refreshed UI using libhandy
> ![](cde1fdff762c90e3134d1e926896a6a375e4f32f.png)

# Third Party Projects

[Forever](https://matrix.to/#/@foreverxmld:matrix.org) announces

> Random 1.4 was released. It completely rehauls the Roulette view, and makes the app easier to use. You can grab it off [Flathub](https://beta.flathub.org/apps/details/page.codeberg.foreverxml.Random).
> {{< video src="BNAVGnsCjiadfJHGCXvthfvh.webm" >}}

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> Workbench - the sandbox to learn and prototype with GNOME technologies had a new release featuring
> * Take a png screenshot of the preview
> * The console is now resizable
> * Redesign the dark/light mode switcher
> * Show platform and dependencies in about dialog
> * Update to GNOME platform 42
> * Various fixes
> 
> https://flathub.org/apps/details/re.sonny.Workbench
> ![](MkmxYCWoEDCMoQsYYCVlBvBJ.png)

### Portfolio [↗](https://github.com/tchx84/Portfolio)

A minimalist file manager for those who want to use Linux mobile devices.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) announces

> Portfolio 0.9.14 released! Improved UX for unlocking devices and a few bug fixes. See all the details [here](https://github.com/tchx84/Portfolio/blob/master/CHANGELOG.md#0914-2022-03-19).
> ![](RcZlQTTecdgmoVYHVzAiQkQJ.gif)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@guido.gunther:talk.puri.sm) reports

> I've tagged phosh 0.17.0 adding a mobile data indicator and support for `org.freedesktop.impl.portal.Access`:
> ![](XljQdPDmyXYxMJrxqvEymOaR.png)

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Julian Sparber](https://matrix.to/#/@jsparber:gnome.org) announces

> Hello folks, quick update on what major things happened in Fractal-next the last two month. The most exciting addition is definitely the SSO support we merged this week and therefore we could  close a 2 years old issue.
> 
> Timeline
>  - You can now send files via drag-n-drop and via the file send button to a room. It also includes a nice preview for Images.
>  - The timeline now shows audio messages with a small inline player.
>  - Fractal-next lets you now remove messages you sent
> 
> Session verification
>  - During first login, Fractal checks if the user hasn't started session verification from another client before offering to start a new one
>  - The QrCode scanning is now spec compliant,  and asks for user's confirmation after scanning.
>  - We dropped screenshot support for QrCode scanning, since it makes the UX worse without adding any real benefit. 
> 
> Room details
>  - The room details show now the members of the room including the power level
> 
> Login
>  - Fractal-Next now supports SSO 🎉️
>  - We implemented auto-discovery of the homeserver via .well-known
> ![](762d6f95db9c7e5acf8f0cb4bfee30ed794c9c9e.png)
> {{< video src="9090c597538f66820e413273bd7c142ef6e195db.webm" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
