---
title: "#160 Web Happenings"
author: Felix
date: 2024-08-09
tags: ["gtk-rs", "epiphany", "gnome-network-displays", "mutter", "webkitgtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 02 to August 09.<!--more-->

# Sovereign Tech Fund

[Thib](https://matrix.to/#/@thib:ergaster.org) announces

> it's been a little while since you received STF updates, but here is a high-level status of what is being worked on and why!
> 
> The Sovereign Tech Fund contracted with the GNOME Foundation with those 8 objectives:
> 
> 1. Encrypt user home directories individually
> 2. Increase the range an quality of hardware support
> 3. Modernize platform infrastructure
> 4. Improve QA and development tooling
> 5. Improve the state and compatibility of accessibility
> 6. Design a new accessibility stack for the Linux Desktop
> 7. Maintenance and modernization of security components
> 8. Secure APIs for Wayland/Flatpak
> 
> There is work going on in several areas, but for the sake of brevity, here are some highlights from the month of July. 
> 
> ## Encrypt user home directories individually
> 
> On Linux systems, users are traditionally managed in a specific file called `/etc/passwd`. It doesn't make a difference between technical and human accounts. It also only stores very basic information about users, and can only be modified by root. It's a very rigid users database.
> 
> AccountsService is a "temporary" daemon (since 2010) bringing to manage users and store more extensive information about them: profile picture, parental control settings, preferred session to log into, languages, etc. It manages `/etc/passwd` and its own database.
> 
> homed is a modern replacement for AccountsService. It provides encryption for user data, and paves the way for future exciting platform security and flexibility improvements. Adrian made AccountsService integrate with homed in order to make the transition easier
> 
> He got the [homed support for AccountsService MR](https://gitlab.freedesktop.org/accountsservice/accountsservice/-/merge_requests/146) merged. This MR was blocking all the homed MRs from landing.
> 
> ## Modernize platform infrastructure
> 
> ### GTK and libadwaita
> 
> GTK is a popular development toolkit to create graphical apps in the free desktop. libadwaita is a library based on GTK providing the building blocks for modern GNOME applications.
> 
> Alice [opened an issue about platform library interface](https://gitlab.gnome.org/GNOME/gtk/-/issues/6821). She also did libadwaita releases: [1.4.7](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1235), [1.5.3](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1234), [1.6beta](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1236), [1.6rc](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1237).
> 
> She fixed issues on Dialog before beta:
> 
> - [https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1231](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1231)
> - [https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1230](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1230)
> 
> ### Notifications
> 
> There are multiple options for apps to send notifications to the system, each with different features and constraints. Julian works on unwrapping the current situation and turning the XDG portal notification API into the go to standard for notifications with a focus on accessibility, consistency across different desktops, extensibility and modern features. Julian works on improving the platform API for displaying notifications on GNU/Linux desktops with main focus on GNOME Shell.
> 
> ### Intents System
> 
> Several platforms offer for native apps to open https links (e.g. opening maps.google.com links in the Google Maps app). The free desktop doesn't anything other than registering as handlers for custom schemes. Andy is working on an intents system to supports https url.
> 
> ## Improve quality assurance and development tooling
> 
> systemd-sysupdate is an update system allowing immutable systems to apply lightweight delta updates. It helps image-based systems support immutability, auto-updates, adaptability, factory reset, uniformity and providing a trust chain from the bootloader all the way up.
> 
> sysupdate is a CLI tool. In order to be able to use systemd-sysupdate, sturdier services are needed. sysupdated is a service that provides a dbus API for sysupdate.
> 
> systemd-repart is a tool that systemd runs on boot that non-destructively repartitions disks.
> 
> Varlink is a kinda simplistic IPC protocol that works early during boot, before the dbus broker has started.
> 
> Adrian Vovk is making steady progress on sysupdate, sysupdated, and Varlink support for systemd-repart.
> 
> ## Improve the state and compatibility of accessibility
> 
> Joanie is cleaning up Orca's code and supporting new keygrabs.
> 
> ## Maintenance and modernization of security components
> 
> The free desktop standardises the storage and usage of secrets (such as passphrases or SSH keys) via the secrets specification. gnome-keyring was the backend implementation, and libsecret the client-side of said specification.
> 
> gnome-keyring and libsecret are written in C and lack maintenance. oo7 is a modern Rust client-side library that respects the secrets specification. Dhanuka is extending oo7 to implement the backend side of secrets management, and ultimately replace gnome-keyring
> 
> ## Secure APIs for Wayland/Flatpak
> 
> Georges [released xdg-desktop-portal-gnome](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/tags/47.alpha). He started documenting the Flatpak a11y situation (and why it's currently unresolvable).
> 
> He also merged a11y-related patches for xdg-dbus-proxy ([https://github.com/flatpak/xdg-dbus-proxy/pull/61](https://github.com/flatpak/xdg-dbus-proxy/pull/61)) + flatpak ([https://github.com/flatpak/flatpak/pull/5828](https://github.com/flatpak/flatpak/pull/5828)) + WebKit ([https://github.com/WebKit/WebKit/pull/29052](https://github.com/WebKit/WebKit/pull/29052)).
> 
> He finished and merged GLib documentation updates branch: [https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4113](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/4113)
> 
> And finally he merged gnome-shell and xdg-desktop-portal-gnome bits of the Notifications rework that Julian had worked on.
> - [https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/170](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/merge_requests/170)
> - [https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3382](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3382)

# GNOME Core Apps and Libraries

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[Michael Catanzaro](https://matrix.to/#/@mcatanzaro:gnome.org) says

> I have fixed [a notable bug in WebKitGTK](https://bugs.webkit.org/show_bug.cgi?id=262794) that prevented cached (unused) subprocesses from suspending properly. WebKit would accidentally suspend the bwrap or flatpak-spawn supervisor process instead of the actual WebKit subprocess. With its supervisor process suspended, the actual WebKit subprocess would remain even after the application is closed, wasting memory. This fix will be present in WebKitGTK 2.44.3.

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[Jan-Michael Brummer](https://matrix.to/#/@jbrummer:matrix.org) reports

> GNOME Web finally received an automatic form filler implementation based on Abdullah Alansari initial work.
> ![](QMmQDyOYsHVbeeDcVGnVhYFg.png)

[Michael Catanzaro](https://matrix.to/#/@mcatanzaro:gnome.org) reports

> Arak has landed several commits improving sorting of bookmarks in Epiphany 47.beta! The previous mysterious sort order has been replaced with alphabetical case-insensitive sort, with an exception for Favorites bookmarks, which sort before non-Favorites.

[Michael Catanzaro](https://matrix.to/#/@mcatanzaro:gnome.org) announces

> Jan-Michael Brummer added a Privacy Report dialog to Epiphany 47.beta to better show how the Intelligent Tracking Prevention feature works. You can now see which tracking domains have been blocked on which websites, and conversely on which websites a tracking domain has been blocked. This doesn't make any changes to the workings of Intelligent Tracking Prevention, which has been supported since Epiphany 3.38; it just provides new visibility into what it is doing.
> ![](07d91c469d845287bc9320b004d03bffe5da398b1819471524600479744.png)

### Mutter [↗](https://gitlab.gnome.org/GNOME/mutter/)

A Wayland display server and X11 window manager and compositor library.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> I spent the last weekend doing some code reshuffling around the accessibility bits of Mutter / GNOME Shell, documented some parts of it in https://belmoussaoui.com/blog/19-accessibility-adventure/
> ![](cc87560d6967efa817a84e387ae6a5a7ffc0877e1821215114456465408.png)

### libspelling

[hergertme](https://matrix.to/#/@spylor:matrix.org) reports

> libspelling got support for spellchecking off of the main thread to improve performance of large documents. Text Editor and Builder have both been updated to use libspelling for spellchecking features.

# GNOME Circle Apps and Libraries

### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[slomo](https://matrix.to/#/@slomo:matrix.org) reports

> gtk4-rs 0.9 and gtk-rs-core 0.20 were just freshly released, just in time for also being included in the GNOME 47 release.
> 
> This release is again relatively small, mostly providing bindings to new APIs and improvements to the glib::clone! and glib::closure! macros to work better with cargo fmt and rust-analyzer.
> 
> As usual, at the same time gstreamer-rs 0.23 and gst-plugins-rs 0.13, libadwaita 0.7 and other related crates got a new release compatible with the new gtk4-rs and gtk-rs-core releases and their own set of changes.
> 
> See https://gtk-rs.org/blog/2024/07/17/new-release.html for more details.

# Third Party Projects

[Ronnie Nissan](https://matrix.to/#/@ronniedroid:mozilla.org) says

> This week I releases v0.3.0 of Embellish, a simple nerd fonts manager, the updates include:
> 
> * Upgrade to libadwaiata 1.5
> * Migrate to AboutDialog
> * Separate installed and available fonts
> * Consolidate the install and uninstall buttons.
> 
> You can get Embellish on [flathub](https://flathub.org/apps/io.github.getnf.embellish)
> ![](0799876ad24213b436ac022b416ede1b0dc5611d1819980603584413696.png)

### GNOME Network Displays [↗](https://gitlab.gnome.org/GNOME/gnome-network-displays)

Stream the desktop to Wi-Fi Display capable devices

[lorbus](https://matrix.to/#/@lorbus:matrix.org) reports

> GNOME Network Displays (https://gitlab.gnome.org/GNOME/gnome-network-displays/) v0.93.0 is out!
> GND allows for screencasting a window, display or virtual display to Miracast and Chromecast devices.
> With this release, a couple of issues were fixed and support for VAH264 gstreamer hardware encoding was added.
> GND is looking for new contributors to help finish the daemonification and dbus API addition efforts that were started as part of last year's GSoC with the goal of integrating it more tightly with GNOME Shell and GNOME Control Center.
> Give it a try, user feedback is also very much appreciated! 
> Stable releases are available on Flathub and nightly builds in the GNOME Nightly Flatpak repo.

# Internships

[IlChitarrista](https://matrix.to/#/@ilchitarrista:matrix.org) announces

> As the last third of GSoC starts we're almost done with the Ignored Apps implementation, it will soon be possible to make some apps local only.
> Drivers and Development Tools will also be excluded from synchronization by default as they are automatically installed as necessary.
> Many thanks for the design feedback by [Jamie Gravendeel](https://gitlab.gnome.org/monster), Adwaita and GTK suggestions by [Alice Mikhaylenko](https://gitlab.gnome.org/alicem) and code review by my mentor [Cogitri](https://gitlab.gnome.org/Cogitri).
> If you're interested in following development, [here's the merge request](https://gitlab.gnome.org/Cogitri/flatsync/-/merge_requests/64).
> 
> As we're ahead of schedule, we're now starting to look into the necessary steps for a future release on Flathub and inclusion in the GNOME Circle!
> {{< video src="CPxyQBhkJdwXuabSsDHQQZMy.webm" >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
