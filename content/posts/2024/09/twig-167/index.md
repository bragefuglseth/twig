---
title: "#167 Linking Apps"
author: Felix
date: 2024-09-27
tags: ["fractal", "auto-activities", "mousai", "parabolic", "libadwaita", "glib"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 20 to September 27.<!--more-->

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) says

> a few more additions in libadwaita:
> 
> `AdwNavigationSplitView` now has the same [`:sidebar-position:`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.NavigationSplitView.sidebar-position.html) property as `AdwOverlaySplitView`, inverting the navigation when collapsed (content as the root page, sidebar as the subpage)
> 
> `AdwNavigationView` got [horizontal](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.NavigationView.hhomogeneous.html) and [vertical](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.NavigationView.vhomogeneous.html) homogeneous properties, meaning it will preallocate the size needed to display any of the added pages, as well as any pages within the navigation stack, rather than just the currently visible page
> 
> `AdwAboutDialog` now has [API](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/method.AboutDialog.add_other_app.html) for linking to your other apps directly from the dialog
> ![](7dff392f49322b9ccc27e49b6e3d08631f29b78a1839730794940071936.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Christian Hergert has gone on a profiling rampage in `GVariant` and significantly reduced its memory allocations when handling smaller variants (which is very common); see https://gitlab.gnome.org/GNOME/glib/-/merge_requests?scope=all&state=all&author_username=chergert&milestone_title=2.83.0

# GNOME Circle Apps and Libraries

### Mousai [↗](https://github.com/SeaDve/Mousai)

Identify songs in seconds.

[Dave Patrick](https://matrix.to/#/@sedve:matrix.org) reports

> [Mousai](https://github.com/SeaDve/Mousai) 0.7.8 has just been released on [Flathub](https://flathub.org/apps/io.github.seadve.Mousai), featuring several improvements:
> * The lyrics are now in its own view.
> * Navigation page names are now more consistent.
> * Song bar now adapts to smaller screens.
> * The overall package size is now smaller, thanks to cargo vendor filterer.
> * Loading buttons are now more modern, thanks to the new Adwaita widgets.
> ![](VZccFAYTseJpZClbOukFhoHi.png)

# Third Party Projects

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> **Aurea** has released version 1.5, featuring several exciting new updates:
> 
> * GNOME 47 support
> * Hot reload banner when metainfo is edited
> * Open metainfo file using Aurea
> * Reload banner using &lt;F5&gt;
> * Add Norwegian translations
> * Blueprint 0.14.0
> 
> Support me on [Ko-fi](https://ko-fi.com/cleomenezesjr)
> Check it out on [Flathub](https://flathub.org/apps/io.github.cleomenezesjr.aurea)!
> {{< video src="YYYVgEYqslRgcziiyxKgCXzR.mp4" >}}

[xjuan](https://matrix.to/#/@xjuan:gnome.org) announces

> I am pleased to announce a new Cambalache stable release, version 0.92.0!
> 
> What's new:
>  - Basic port to Adwaita
>  - Use Casilda compositor for workspace
>  - Update widget catalogs to SDK 47
>  - Improved Drag&Drop support
>  - Improve workspace performance
>  - Enable workspace animations
>  - Support new desktop dark style
>  - Support 3rd party libraries
>  - Streamline headerbar
>  - Lots of bug fixes and minor improvements
> 
> Read more about it at https://blogs.gnome.org/xjuan/2024/09/26/new-cambalache-release-0-92-0/
> ![](7453f9cfaec1f6e90674602727ac3eb99fe7fb6d1839654630376603648.png)

[Dave Patrick](https://matrix.to/#/@sedve:matrix.org) reports

> [Delineate](https://flathub.org/apps/io.github.seadve.Delineate), formerly known as Dagger, has just been released. This sleek new app allows is designed for editing and viewing graphs using the [DOT Language](https://graphviz.org/doc/info/lang.html). For all the details and features, take a look at the [release blog post](https://seadve.github.io/blog/11-introducing-delineate/).
> ![](EetQWOXMeuOhLotlSWCTUFoy.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Parabolic [V2024.10.0-rc1](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.10.0-rc1) is here!
> 
> Compared to V2024.9.0-beta3, this release adds a new `Preferred Video Codec` option in Preferences, an improved format selection backend, a new subtitles selection interface for individual downloads, and the ability to copy the command used to run a download when viewing its log.
> 
> These are the final changes that we intend to make for this release cycle and are aiming to release a stable version on Wednesday October 2.
> 
> Here's the full changelog for this release cycle:
> 
> * Parabolic has been rewritten in C++ for faster performance
> * The Keyring module was rewritten. As a result, all keyrings have been reset and will need to be reconfigured
> * Audio languages with audio description are now correctly recognized and handled separately from audio languages without audio description
> * Audio download qualities will now list audio bitrates for the user to choose from
> * Playlist downloads will now be saved in a subdirectory with the playlist's title within the chosen save folder
> * When viewing the log of a download, the command used to run the download can now also be copied to the clipboard
> * The length of the kept download history can now be changed in Preferences
> * On non-sandbox platforms, a browser can be selected for Parabolic to fetch cookies from instead of selecting a txt file in Preferences
> * Added an option in Preferences to allow for immediate download after a URL is validated
> * Added an option in Preferences to pick a preferred video codec for when downloading video media
> * Fixed validation issues with various sites
> * Fixed an issue where a specified video password was not being used
> * Redesigned user interface
> * Updated yt-dlp
> ![](hIyGGYLASnDAfYrhXIcozkwR.png)
> ![](oBjaZOfynzpjUzxozxPNbAPB.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> Back to school, and Fractal is back too! The leaves are starting to cover the floor in our part of the globe, but you don’t have to shake a tree to get our goodness packed into Fractal 9.beta:
> 
> * We switched to the glycin library (the same one used by GNOME Image Viewer) to load images, allowing us to fix several issues, like supporting more animated formats and SVGs and respecting EXIF orientation.
> * The annoying bug where some rooms would stay as unread even after opening them is now a distant memory.
> * The media cache uses its own database that you can delete if you want to free some space on your system. It will also soon be able to clean up unused media files to prevent it from growing indefinitely.
> * Sometimes the day separators would show up with the wrong date, not anymore!
> * We migrated to the new GTK 4.16 and libadwaita 1.6 APIs, including CSS variables, AdwButtonRow and AdwSpinner.
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, there might be a slight risk of regressions, but it should be mostly stable. If all goes well the next step is the release candidate!
> 
> If you have a little bit of time on your hands, you can try to fix one of our [newcomers issues](https://gitlab.gnome.org/World/fractal/-/issues/?label_name%5B%5D=4.%20Newcomers). Anyone can make Fractal better!
> ![](1f5f3ae34a5283f89c2dcd72ebcfd071fe6e20fa1839333565091807232.png)

# Shell Extensions



### Auto Activities [↗](https://extensions.gnome.org/extension/5500/auto-activities/)

Show activities overview when there are no windows, or hide it when there are new windows.

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> **Auto Activities** received support for GNOME Shell 47.
> 
> Get it on [extensions.gnome.org](https://extensions.gnome.org/extension/5500/auto-activities/)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
