---
title: "#164 Updated Translations"
author: Felix
date: 2024-09-06
tags: ["parabolic", "turtle"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 30 to September 06.<!--more-->

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> Thank you to the translation teams for all the translation updates which happen in GNOME, particularly just before a release, like just now. It is really appreciated!

# Third Party Projects

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> A wild new [stable release](https://gitlab.freedesktop.org/cairo/cairo/-/releases/1.18.2) of Cairo appeared! It's super-effective at fixing build issues and bugs on a variety of platforms and toolchains. Thanks to Federico Mena, Cairo now generates static analysis and coverage reports as part of the CI pipeline; the coverage reports are [published](https://cairo.pages.freedesktop.org/cairo/coverage/), so if you want to contribute to the Cairo project you can now find where your changes can be most effective.

### Turtle [↗](https://gitlab.gnome.org/philippun1/turtle)

Manage git repositories in Nautilus.

[Philipp](https://matrix.to/#/@philippun:matrix.org) reports

> [Turtle](https://flathub.org/apps/de.philippun1.turtle) [0.10](https://gitlab.gnome.org/philippun1/turtle/-/releases/0.10) has been released.
> 
> There have been a lot of fixes and refactoring, notable changes:
> 
> # Credential changes
> 
> For SSH repos ssh-agent will now be used by default. This makes it easier to use different keys with or without password protection. You can still configure a predefined ssh key in the settings.
> 
> It is now possible to use HTTPS repos with username and password. A userpass dialog will prompt for your credentials which can be stored in the gnome keyring. I was not able to test this feature myself, because I do not have access to a repository which allows HTTPS with password. [But it seems to work](https://gitlab.gnome.org/philippun1/turtle/-/issues/23#note_2207572).
> 
> # File-manager plugin changes
> 
> A huge bottleneck, which caused nautilus to freeze up and worst case to crash has been fixed.
> 
> Unfortunately I found another [issue](https://gitlab.gnome.org/GNOME/nautilus/-/issues/3505) with the `update_file_info_full` function, which I can reproduce on multiple distros. For the time being the turtle plugin uses `update_file_info` instead. This makes emblem calculation slower, but at least it runs stable again.
> 
> There is now a turtle emblem which will be shown for the repo main folder by default. The status emblem can be activated again in the settings. It is also possible to show both or none of them. This change further speeds up emblem calculation in folders with many (100+) repos, especially with the workaround mentioned above, and also makes submodules more visible.
> ![](sDwKpvGAudMNFpxOYxvdqqwJ.png)
> ![](IzSSnYWRiqoCSuWPEjeIryzz.png)

### Parabolic [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Download web video and audio.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Parabolic [V2024.9.0-beta1](https://github.com/NickvisionApps/Parabolic/releases/tag/2024.9.0-beta1) is here! 
> 
> After a long awaited couple of months, Parabolic has been rewritten in C++ and features a redesigned user interface! Users should expect a faster and more reliable downloader.
> 
> We encourage all Parabolic users to give this beta a try and iron out all issues before the stable release (targeted for next week).
> 
> Here's the full changelog:
> * Parabolic has been rewritten in C++ for faster performance
> * The length of the kept download history can now be changed in the app's preferences
> * Cookies can now be fetched from a selected browser in Preferences instead of selecting a TXT cookies file
> * Parabolic's Keyring module was rewritten. As a result, all keyrings have been reset and will need to be reconfigured
> * Fixed validation issues with various sites
> * Fixed an issue where a specified video password was not being used
> * Redesigned user interface
> * Updated yt-dlp
> ![](vdLVDoZIMnhQoLopMKQMhEGw.png)
> ![](ySOoOhhEGJMzNmodphuqXduJ.png)

# Shell Extensions

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) says

> Is it hot in the Northern Hemisphere and cold in the Southern Hemisphere?
> _Weather O'Clock_ has been ported to GNOME Shell 47! With this new version, it is possible to customize the display of weather information, choosing whether it appears before or after the clock.
> {{< video src="FnetrqbalkapUTpjnEOdZDKK.mp4" >}}

[Arca](https://matrix.to/#/@arcaege:matrix.org) says

> New updates for the Day Progress extension - circular indicators and GNOME 47!
> 
> _Day Progress_, the extension that lets you visualise how much time is left of your day has now been ported to support GNOME 47! There is now an (experimental) option to display the time elapsed/remaining as a circular ("pie") indicator too. I haven't run into any bugs with the new indicator style in my few days of testing but I'm still hesitant to call it stable, that's why I have labelled it as 'experimental'. You can download the extension at https://extensions.gnome.org/extension/7042/day-progress/ and the repository is available at https://github.com/ArcaEge/day-progress where you can also report any issues you find.
> ![](RzPmpPuhAdnBxbVnGxaGRBwH.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
