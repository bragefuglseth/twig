---
title: "#155 Overhauled Keyrings"
author: Felix
date: 2024-07-05
tags: ["gdm-settings", "fractal", "webkitgtk", "flatsync", "keypunch", "phosh"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from June 28 to July 05.<!--more-->

# Sovereign Tech Fund

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) says

> [Key Rack 0.4](https://flathub.org/apps/app.drey.KeyRack) has been released. This release contains all the work that Felix has done as part of STF. The version brings a completely overhauled user interface. But the changes go much further than a better look. Key Rack now allows to view all host keyrings as well to change passwords of keyrings. Passwords can now be found more easily via the new search function and new passwords can be added via the interface.
> 
> You can use Key Rack for to lookup password that apps have store or use it as a personal password manager with multiple keyrings.
> ![](e74dce609202a9b8aa49869d523c436a19c2d9591809288270232682496.png)

# GNOME Core Apps and Libraries

### WebKitGTK [↗](https://webkitgtk.org/)

GTK port of the WebKit rendering engine.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> WebKit can now be profiled using Sysprof on Linux. This includes both the WebKitGTK and the WPE WebKit ports. WebKit is the first web engine on Linux to integrate with Sysprof!
> 
> For now, this integration exposes all pre-existing tracing points in WebKit. This includes JSC, WebProcess, and more. New tracing points are being considered for addition.
> 
> This is an important step for WebKit on Linux as it open the door for potential performance improvements, reduce resource usage, and more.
> ![](3e4cc632a1f604df93e13cb29703a3c5d3910f0d1809314527926288384.png)

# Third Party Projects

[elvishcraftsman](https://matrix.to/#/@elvishcraftsman:matrix.org) says

> I've created my first GNOME app, Time Tracker, which is now [available on Flathub](https://flathub.org/apps/com.lynnmichaelmartin.TimeTracker). 
> 
> Time Tracker is local-first and can sync through cloud storage (saving to a CSV file). This makes it able to load significantly faster than online-first time tracking software. 
> 
> Time Tracker supports custom filtering, where it reports on the amount of time spent during a particular date range and by project.
> ![](MvVJcgqZgIJSjaVasYCTUFLl.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> Phosh 0.40.0 is out:
> 
> [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) got (optional) dark style an mobile data quick toggles. One can suspend from the lock screen now and we have a bunch of bug and UI fixes. [Phoc](https://gitlab.gnome.org/World/Phosh/phoc) now supports a fling gesture to make opening and closing the panels a bit nicer.
> 
> There's more. Check the full details [here](https://phosh.mobi/releases/rel-0.40.0/)
> ![](uMSEBdEhkxOQgEdpGKCRfcXz.png)

### Keypunch [↗](https://github.com/bragefuglseth/keypunch)

Practice your typing skills

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> Bonjour! Sziasztok! 안녕! This week I released Keypunch 2.0. Get ready to practice typing in many new languages.
> - Text generation support for Bulgarian, Hindi, Korean, Nepali, Russian, Swiss German, and Ukranian
> - User interface translations for Bulgarian, Czech, Dutch, French, German, Hindi, Hungarian, Spanish and Ukranian
> - More accepted ways to type certain characters, such as “oe” for “œ”
> - Better handling of scripts using intelligent input engines, like Korean Hangul
> - Recognition of Ctrl+Backspace
> - Special accomodations for on-screen keyboards and touch screens
> 
> A special thanks to everybody who has contributed orthographic information and tested the text generation and input machinery for their languages. Keypunch’s diverse language support would not be possible without you.
> You can get Keypunch on [Flathub](https://flathub.org/apps/dev.bragefuglseth.Keypunch).
> ![](d49857117fdd7297f90f8984dae7ffa212d205e61807759613437673472.png)

### GDM Settings [↗](https://gdm-settings.github.io)

Customize your login screen.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> I am calling for contributors for GDM Settings. Lately, I have been busy in my day job, and haven't had much time to work on GDM Settings.
> 
> About 2 months ago, I requested the users for donations for the development of the app. At the time of writing this, we have $179.47 USD in our OpenCollective. I know it is not much, but meaningful contributions may be rewarded with little gifts (payouts) in the range of $30 to $50. If you want to work on something specific for some fixed amount of money, you can do that as well. Just reach out to me in [GitHub Discussions](https://github.com/orgs/gdm-settings/discussions).
> 
> If you have any questions or suggestions, please, don't hesitate to reach out in [GitHub Discussions](https://github.com/orgs/gdm-settings/discussions).

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) reports

> It’s been all over the news, so you probably already have heard about what’s happening in France, but in case you haven’t yet: a new Fractal beta just dropped!
> 
> What have we been up to for Fractal 8.beta?
> 
> * Mentions are sent intentionally
> * Authenticated media are supported
> * Draft messages are kept per-room
> * The verification and account recovery processes have been polished
> * HTML rendering should be more robust, as we switched to the same library already used by the Rust SDK to cleanup tags in messages
> * Speaking of HTML rendering, code blocks gained syntax highlighting, lists can be nested, numbered lists can start from an arbitrary number, the details tag is supported, and @room mentions are detected and rendered with a pill
> 
> As usual, this release includes other improvements, fixes and new translations thanks to all our contributors, and our upstream projects.
> 
> It is available to install via Flathub Beta, see the [instructions in our README](https://gitlab.gnome.org/World/fractal#installation-instructions).
> 
> As the version implies, there might be a slight risk of regressions, but it should be mostly stable. If all goes well the next step is the release candidate!
> 
> As always, you can try to fix one of our [issues](https://gitlab.gnome.org/World/fractal/-/issues). Any help is greatly appreciated!
> ![](3cddb8d8968e7087f98106b6d764291ea15bf9e81808588065434763264.png)

# Internships

### FlatSync [↗](https://gitlab.gnome.org/Cogitri/flatsync)

Keep your Flatpak apps synchronized between devices

[IlChitarrista](https://matrix.to/#/@ilchitarrista:matrix.org) announces

> We’ve finally landed the Initial Setup as part of GSoC, now FlatSync is configurable through a nicely designed UI.
> Many thanks for the feedback throughout development from my mentor [Cogitri](https://gitlab.gnome.org/Cogitri) and various people in the Design channel.
> 
> Now I’m starting to work on implementing Filtering to bring more granular control over FlatSync’s behaviour.
> {{< video src="pDHiLwacFWSLYdYyOEUjEZgj.mp4" >}}

# GNOME Foundation

[Allan Day](https://matrix.to/#/@aday:gnome.org) says

> The GNOME Foundation Board of directors has been getting its new members up and running this week. Newly elected directors have been joining the board Matrix channels, and training and briefing sessions have been scheduled for next week. Planning is also underway for the 2024 board annual meeting, which will happen ahead of GUADEC, on 17 July.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
