---
title: "#136 New Papers"
author: Felix
date: 2024-02-23
tags: ["eartag", "gaphor"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 16 to February 23.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> ### Accessibility
> 
> Joanie continued improving the Orca screen reader:
> 
> * Finished removing all pyatspi code 🍾
> * New Keyhandling/Grabs
>   * Event Manager: Add means to pause and clear the event queue: https://gitlab.gnome.org/GNOME/orca/-/commit/967a32407
>   * Check for device before adding a grab: https://gitlab.gnome.org/GNOME/orca/-/commit/c4a7b391f
> * Remove/Move speech-related hacks
>   * See related https://gitlab.gnome.org/GNOME/orca/-/merge_requests/182#note_2009083
>   * speech-dispatcher: Remove hack for newline followed by period: https://gitlab.gnome.org/GNOME/orca/-/commit/3a4001df6
>   * speech-dispatcher: Move `u00a0`-> to adjustForPronunciation: https://gitlab.gnome.org/GNOME/orca/-/commit/1957edb8a
> * Event spam + code clean up
>   * Discovered and filed https://gitlab.gnome.org/GNOME/gtk/-/issues/6449
>   * Took the opportunity to clean up the Orca code that handles such issues: https://gitlab.gnome.org/GNOME/orca/-/commit/8faea2f7f
>   * Ignore checked-changed events from non-showing widgets: https://gitlab.gnome.org/GNOME/orca/-/commit/48ebd3db2
> 
> 
> Georges changed how WebKit makes accessible objects implement `Hypertext`, `Hyperlink`, and `Text`. This should unblock further accessibility work for Joanie and Orca: https://github.com/WebKit/WebKit/pull/24956
> 
> 
> Andy landed Spiel text-to-speech support in Orca:
> 
> * Update Orca's Spiel server along with change to upstream API:
>     - SpielProvider changes: https://gitlab.gnome.org/GNOME/orca/-/merge_requests/182/diffs?commit_id=4db5f96178652298b563c9e829689952cdc0fb4b
>     - New events: https://gitlab.gnome.org/GNOME/orca/-/merge_requests/182/diffs?commit_id=b1e66999690d88bb4cde5093ec6d958472cc80d8
>     - Build Spiel from upstream: https://gitlab.gnome.org/GNOME/orca/-/commit/a93d512c3f37511c90c8aab68c14e1c8e526b722
> 
> 
> ### Security
> 
> Dhanuka continued work on implementing oo7-daemon:
> 
> * Opened a new PR to faciliate collaboration with Bilal https://github.com/bilelmoussaoui/oo7/pull/73
> * Make Algorithm enum public under unstable feature: https://github.com/bilelmoussaoui/oo7/pull/72
> * Removed `OnceLock<Keyring>` and introduced `Service::new()` to initialize the Service containing `oo7::portal::Keyring`
> * Ported to zbus 4.0
> 
> ### New Accessibility Stack (Newton)
> 
> Matt is working on the AT-SPI compatibility library for Newton.
> 
> * Extracted AccessKit AT-SPI implementation into a core library https://github.com/AccessKit/accesskit/pull/352 as a prerequisite
> 
> ### Hardware Support
> 
> Jonas continued his work on various GNOME Shell things that are still on track for 46, including hardware encoding for screencasts and the new gesture API:
> 
> * Debugged some more gnome-shell screencast issues with HW encoding and iterated on the MR (https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2080), it will hopefully land once the gstreamer pipelines get an ack from the gstreamer folks
> * Rebased gestures part 2 MR: https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/2389
> * Helped the Outreachy students with mobile QA
> 
> Jonas continued to push fractional scaling forward:
> 
> * Almost finished (only needs more tests) with the mutter monitor config work to allow toggling on "scale-monitor-framebuffer" by default: https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3596
> * Sketched out two algorithms for migrating monitor configs in mutter and implemented them (see https://gitlab.gnome.org/GNOME/mutter/-/commit/31c3d647d9ac792cc3b07f653dd4dcba75b8fc32?merge_request_iid=3596 for fancy ASCII art)
> * Updated Jonas Ådahl's MR to g-s-d for Xwayland scaling: https://gitlab.gnome.org/GNOME/gnome-settings-daemon/-/merge_requests/353
> * Opened an MR to g-c-c for to configure Xwayland scaling: https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2286
> * Worked with Tobias and Sonny on design for Xwayland scaling option in g-c-c
> 
> Dor continued his work on variable refresh rate support:
>     
> * Made Mutter's libdisplay-info dependency more visible to downstream packagers
>     - Among other features, this is useful for https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3576
>     - Mutter MR: https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3582
>     - g-b-m MR: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2671
> * Rebased MRs on GNOME 46 beta and made minor changes following review comments:
>     - https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1154
>     - https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/734
>     - https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3576
>     - https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2260
> 
> Georges is investigating Nvidia GPU issues with WebKitGTK
> 
> * Ran a variety of WebKitGTK tests, builds, and apps on Nvidia
> * Reduced the scope of the bugs as a lot more seems to be functional now.
> * GTK3 and GStreamer videos might still have problems, working on it
>   - https://bugs.webkit.org/show_bug.cgi?id=228268
>   - https://bugs.webkit.org/show_bug.cgi?id=261874
> 
> :information_source: we are trying to make things work but this is not an endorsement that you should use Nvidia on Linux :)
> 
> ### Platform
> 
> Alice fixed some minor issues before the libadwaita 1.5 release
> 
> * Fixed dialog dimming; fixed shade colors in dark in general in process: https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1052
> * Fixed .devel styles on dialogs: https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1051
> 
> Evan worked on merging the two GNOME TypeScript bindings https://github.com/gjsify/ts-for-gir/pull/144
> 
> Evan started prototyping necessary changes in meson to move GNOME repos over to the GLib-based GI compiler
> 
> Andy put some finishing touches on the GNOME Online Accounts GTK4 port:
> * Design discussion: https://gitlab.gnome.org/Teams/Design/settings-mockups/-/issues/68
> * Resulting MRs: https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/merge_requests/178, https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2284
> 
> Julian continued his work on improving notifications:
> 
> * Worked on refactoring notification code in GNOME Shell and expanding notification in calendar drawer https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3173
> * Looked into notification startup id/activation token:
>     - Use correct platform data when activating application actions https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3198
>     - gappinfo: Pass activation token from launch context to open_uri/file portal https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3933
> 
> Hubert is working on the device permission backward compatibility https://github.com/flatpak/flatpak/issues/5681
> 
> Hubert landed memory leak fixes https://github.com/flatpak/flatpak/pull/5683
> 
> ### Home Encryption
> 
> Adrian made major progress on integrating systemd homed for home encryption:
> 
> * Landed blob directories in systemd 🎉 https://github.com/systemd/systemd/pull/30840
> * Fixed GDM hang when trying to unlock user locked & frozen by homed https://gitlab.gnome.org/GNOME/gdm/-/merge_requests/235
> * Fixed blob-dir-related systemd bug: https://github.com/systemd/systemd/issues/31417 https://github.com/systemd/systemd/pull/31419
> * Contributed to design discussion about passwordless users https://gitlab.gnome.org/Teams/Design/settings-mockups/-/issues/66
> * Opened an RFE about the lack of support for auditing in systemd-homed https://github.com/systemd/systemd/issues/31447
> * Brought homed up to feature parity with legacy users in gnome-control-center https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/2306
> * Published a GNOME OS branch for testing and ironed out some isues
>   - https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2681)
>   - https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/-/merge_requests/463
>   - https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/2661
> * Tracked down a systemd regression that completely breaks GNOME https://github.com/systemd/systemd/issues/31287
> * Continue work and addressed review on Homed update policy v2 https://github.com/systemd/systemd/pull/31153
> * Landed new user record language fields, so that we can represent users who can speak multiple languages https://github.com/systemd/systemd/pull/31206
> * Made gnome-initial-setup support homed https://gitlab.gnome.org/GNOME/gnome-initial-setup/-/merge_requests/239)
> * Continued LKML discussion about deleting data out of memory on homed lock https://lore.kernel.org/r/20240116-tagelang-zugnummer-349edd1b5792@brauner
> * Rebased & finished up PR that makes systemd freeze user sessions more aggressively https://github.com/systemd/systemd/pull/30612

# GNOME Incubating Apps

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> [Papers](https://gitlab.gnome.org/GNOME/Incubator/papers) has been accepted into the GNOME Incubator. The GNOME [incubation process](https://gitlab.gnome.org/GNOME/Incubator/Submission) is for apps that are designated to be accepted into [GNOME Core or GNOME Development Tools](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/OfficialAppDefinition.md) if they reach the required maturity.
> 
> Papers is a fork of GNOME's current document viewer Evince. Due to limited resources, the Evince project can currently not facilitate larger changes like the port to GTK 4 and Libadwaita. Those goals will now be pursued as part of the Papers project.
> 
> Papers has already been ported to GTK 4 and Libadwaita. The incubation progress will be [tracked in this issue](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/issues/24).
> ![](248a7383a27df59488887973dfec3a4eff77f91f1761015878721208320.png)

# GNOME Circle Apps and Libraries



### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) says

> Last week Dan Yeaw release [Gaphor](https://gaphor.org/) 2.24.0, the user friendly SysML/UML modeling application. Highlights of this release are:
> 
> * Gaphor is now [REUSE](https://reuse.software/) compliant. This makes it easier for third-parties to build on top of Gaphor.
> * Improvements in the CSS rendering. Styling can be applied much more fine grained.
> ![](tYxOHITiRNuiAiEOMSENrIlV.png)

### Ear Tag [↗](https://gitlab.gnome.org/World/eartag)

Edit audio file tags.

[knuxify](https://matrix.to/#/@knuxify:cybre.space) reports

> [Ear Tag](https://gitlab.gnome.org/World/eartag) 0.6.0 has been released! Among other changes. this release brings:
> 
> * various improvements to the "Rename Selected Files" feature, such as syntax highlighting and the ability to move renamed files to a folder (and create subfolders from tags);
> * a new "Extract Tags from Filename" option that can automatically extract tags based on a pattern;
> *  options to remove all tags from a file or undo all currently pending changes;
> * ...and a few small design tweaks.
> 
> Read more about this release on the [0.6.0 release page](https://gitlab.gnome.org/World/eartag/-/releases/0.6.0), or [get the latest version from Flathub](https://flathub.org/apps/details/app.drey.EarTag).
> ![](KYLhzeZEQuNPyKcaSFeAuszn.png)
> ![](XRXuUnfeXqZOjKUBgKHoOKbB.png)

# Third Party Projects

[Maximiliano 🥑](https://matrix.to/#/@msandova:gnome.org) reports

> search-provider, your favorite Rust zbus-based library to interact with GNOME's SearchProvider, just released its version 0.8.1, new in this version we add a new feature to turn `gdk::Texture`s into icons that you can send over the bus, no more meddling with `gdk_pixbuf::Pixbuf`s!

[slomo](https://matrix.to/#/@slomo:matrix.org) reports

> The GStreamer team is excited to announce the first release candidate for the upcoming stable 1.24.0 feature release.
> 
> This 1.23.90 pre-release is for testing and development purposes in the lead-up to the stable 1.24 series which is now frozen for commits and scheduled for release very soon.
> 
> Depending on how things go there might be more release candidates in the next couple of days, but in any case we’re aiming to get 1.24.0 out as soon as possible.
> 
> Preliminary release notes highlighting all the new features, bugfixes, performance optimizations and other important changes will be available in the next few days.
> 
> If you notice any problems, please file an issue in [GitLab](https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/new).
> 
> https://discourse.gstreamer.org/t/gstreamer-1-23-90-pre-release-1-24-0-rc1
> 
> Thanks for testing!

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) announces

> This week, [Alain](https://github.com/alainm23) unveiled [Planify](https://flathub.org/apps/io.github.alainm23.planify) 4.5, bringing a wave of exciting design enhancements and powerful new features to the popular task management app.
> 
> **Boost your productivity with these innovative additions:**
> 
> * Seamless Nextcloud Integration.
> * Simplified Task Migration from Planner
> * Enhanced Task Management: Experience the convenience of drag-and-drop task movement, allowing you to effortlessly organize your workload with a simple click and drag.
> * Clearer Visibility: Gain valuable insights with the "Always Show Completed Subtasks" option, keeping track of your progress and ensuring nothing falls through the cracks.
> * Efficient Task Creation: Introducing the "Create more" feature, enabling you to swiftly add multiple tasks without interrupting your workflow.
> 
> **Experience a Smoother User Journey:**
> 
> Planify 4.5 doesn't stop there. It boasts a sleekly redesigned date and time selection widget, further enhancing user experience. Additionally, several bug fixes have been implemented to ensure optimal performance and a seamless workflow.
> ![](koilPpwzNVWvzfXFqTaXatQG.png)

# GNOME Websites

[federico](https://matrix.to/#/@federicomena:matrix.org) reports

> GNOME's Code of Conduct has moved from the wiki to https://conduct.gnome.org.  All the informational pages about the CoC Committee, procedures, etc. have moved from the wiki to https://conduct.gnome.org/committee

# Shell Extensions

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) reports

> The [extensions port guide](https://gjs.guide/extensions/upgrading/gnome-shell-46.html) for GNOME Shell 46 is ready. If you need any help with your extension, please ask us on [GNOME Extensions Matrix Channel](https://matrix.to/#/#extensions:gnome.org).

# Miscellaneous

[barthalion](https://matrix.to/#/@barthalion:matrix.org) announces

> Flathub’s automatic build validation is more thorough now, and includes checks for issues we previously would have only flagged manually. We have also started moderating all permission changes and some critical MetaInfo changes. Last but not least, we have switched to libappstream, enabling specifying supported screen sizes for mobile devices, and other features available in the latest spec. More details on our blog: https://docs.flathub.org/blog/improved-build-validation/
> ![](mlKSMeEyNCzQfNVltCGVPTJD.png)
> ![](BOKIDvlAiXoSOkVnGTgSsRRH.png)

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) says

> Following the release of zbus 4.0, I have released a new version of [ashpd](https://crates.io/crates/ashpd) and [oo7](https://crates.io/crates/oo7). The releases consist of mostly bug fixes and reduced dependencies thanks to the zbus update.
> On top of that, [oo7-cli](https://github.com/bilelmoussaoui/oo7/tree/main/cli), a secret-tool replacement is now available to install with `cargo install oo7-cli`

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

