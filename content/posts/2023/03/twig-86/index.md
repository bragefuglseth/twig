---
title: "#86 New Decoding"
author: Felix
date: 2023-03-10
tags: ["flare", "telegrand", "blurble", "loupe", "pika-backup", "share-preview", "workbench"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 03 to March 10.<!--more-->

# GNOME Incubating Apps

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> The new image decoding code has finally landed in Loupe. This change will be the basis for many features to come, like supporting color profiles, animated images, and potentially sandboxed decoding. Already landed has the support for viewing SVGs. Thanks to GTK 4 and tiled rendering, you can view large SVGs without the UI seeing any performance penalties. However, a few known rendering bugs still have to be fixed.
> 
> The GDK-PixBuf support has, for now, been removed from Loupe. Loupe already supports many image formats directly, making it unclear if support for GDK-PixBuf plugins is required. Please let us know if an image format vital to you is missing from the [list of supported formats](https://gitlab.gnome.org/Incubator/loupe#supported-formats).
> 
> Apart from this, we have:
> 
> * Fixed some memory leaks,
> * redone the scroll-wheel logic to support high-resolution scroll wheels,
> * made rotation and pan gestures work correctly on touch screens, and
> * merged many more bug fixes, tweaks, code improvements, and documentation.
> 
> Finally, a small optical change: Drag and drop now features a newly designed thumbnail that provides more contrast to the content behind it.
> ![](1beba251b9a17a12081a837066606c1d6f150b0a.png)

# GNOME Circle Apps and Libraries

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> In just 2 weeks, Workbench gained 11 new GNOME Platform demos and examples. More on the way.
> A huge thanks to all the contributors!
> Library examples and demos have 3 functions
> 
> * Showcase the capabilities of the platform
> * Teach how to use the APIs, patterns, and widgets
> * Provide functional snippets ready to use
> 
> The plan for now is to focus on a wide set of UI / JavaScript entries. In the long run, we want to support more languages into Workbench and port the examples.
> 
> I have written a guide on how to get started contributing to Workbench https://github.com/sonnyp/Workbench/blob/main/CONTRIBUTING.md
> For the time being I will be focusing on stability, mentoring and preparing Workbench 44
> 
> Tobias Bernard Shared their experience using Workbench as a design / prototype tool https://mastodon.social/@tbernard/109972593064382462
> 
> James Westman and I discussed reducing the scope of Blueprint 1.0 in order to drop its experimental status and make it the default syntax in Workbench
> 
> I'd like to give kudos and thanks to GSoC and Outreachy early applicants for their contributions ✨
> ![](9228a4d6ed00a43ed7c1deb90ac2b4089fa39858.png)
> ![](be3515dc8f564ed8ee342a3763e3d08272519a6f.png)
> ![](2069ca2306bab3b49ba1191a80881c9726082eae.png)
> ![](79689552167dd2c8d42b01afafdf250ea247b92d.png)
> ![](aacfe5677eedd62ea3d9e4723954e34fd82de54d.png)

### Share Preview [↗](https://github.com/rafaelmardojai/share-preview)

Test social media cards locally.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Share Preview has a new "logs" feature. The app will now give better hints about errors, missing metadata, and limits set by social platforms like image size.
> 
> This new feature has added a lot of new strings. Everyone is welcome to contribute or update translations for their language on [Weblate](https://hosted.weblate.org/engage/share-preview/).
> ![](HTlIGXiKQmBBIHSZAOWclznT.png)

### Pika Backup [↗](https://apps.gnome.org/app/org.gnome.World.PikaBackup/)

Keep your data safe.

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) announces

> After the release of Pika Backup 0.5, Fina has taken care of many outstanding smaller and more significant issues. Pika Backup now also uses the new background status for apps that will be available with GNOME Shell 44. That way, you can always check what Pika Backup is doing in the background.
> 
> Some of the other changes that happened:
> 
> * Fix compact is not run after pruning
> * Fix potential crash after deleting archives
> * Fix spurious 'Pika Backup crashed' messages
> * Change secret service error messages to include specific instructions how to resolve the issue
> * Change to explain checkpoint creation when aborting backups
> * Change to restart backup after SSH connection timeout
> * Change reconnection to be abortable and count down seconds remaining
> * Add ability to answer questions from borg process
> ![](ea2a07ee17472e089d8ce2bf0e0ee14678c69b17.png)

# Third Party Projects

[Alessandro Iepure](https://matrix.to/#/@aleiepure:matrix.org) announces

> I released Dev Toolbox! If you are tired of using random websites for conversions or simple checks during your coding, give it a try. Simple to use with local processing. Includes encoders and decoders, formatters for various languages, image converters, text and hash generators, and much more. Available on Flathub: https://beta.flathub.org/apps/me.iepure.devtoolbox
> ![](GXhoNwrEsmHajmFsEZXCqawI.png)

[Can Lehmann](https://matrix.to/#/@josh-leh:matrix.org) announces

> This week we released [owlkettle 2.2.0](https://github.com/can-lehmann/owlkettle), a declarative GUI framework based on GTK. Owlkettle is a library for the Nim programming language. One major focus for this release was improving the documentation. There are [15 new examples](https://github.com/can-lehmann/owlkettle/tree/main/examples) as well as a new document explaining how owlkettle works internally. We also improved the support for libadwaita by adding bindings for the EntryRow, ComboRow, ExpanderRow, WindowSurface, Flap and SplitButton widgets. Other improvements include support for multithreading and sending notifications. You can find a short tutorial for getting started with owlkettle [here](https://github.com/can-lehmann/owlkettle/blob/main/docs/tutorial.md).
> ![](QwraNTlDbnGvEJRogBseAuxN.png)

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) says

> [nautilus-code](https://github.com/realmazharhussain/nautilus-code) received support for translations and has already been translated in Hungarian and Italian.

[nxyz](https://matrix.to/#/@nxyz:matrix.org) reports

> This week I released [Chromatic](https://github.com/nate-xyz/chromatic), a simple tuner app written in Rust. Chromatic detects the frequency of audio input, converts it to the correct musical note and octave, and displays the cents error. Cents are displayed on an analog gauge to make tuning more visually intuitive. Requires PulseAudio or PipeWire.
> 
> GH repo: https://github.com/nate-xyz/chromatic
> Flathub page: https://beta.flathub.org/apps/io.github.nate\_xyz.Chromatic
> ![](WpCKEdtFRDDzLweXzsLeGcyD.png)

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) announces

> It's finally time for a new update about Telegrand development! This is a brief look at the new biggest changes from the last update:
> 
> * Added support for file messages by yuraiz
> * Added support for GIF messages
> * Added support for more event-like messages by Alisson Lauffer
> * Added support for viewing message replies by karl0d
> * Added ability to edit and reply to messages
> * Added a Christmast easter egg animation by yuraiz (sorry, we’re late!)
> * Added markdown support for composing messages by karl0d
> * Added more information in the chat info window, like group description, usernames and phone number
> * Added a contacts window to view saved contacts
> * Improved the chat view for channels, by adding a mute/unmute button by karl0d
> * Improved the style of the chat view by yuraiz
> * Big performance improvements to chat view scrolling
> * Ground work for future support of chat folders and archived chats
> ![](f77ddcf9804808daebd96e90ba22ca2a0201e899.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

An unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Flare 0.7.0-beta.1 was released. It does not provide any big new features, but updates many dependencies which lead to pretty big changes in the codebase. To check everything is working as expected, this beta was released for anyone to try out if they want. Note that due to changes in the storage, a relink will be required at the first start of the application.
> 
> Furthermore, Flare has gained experimental support for integrating with [feedbackd](https://source.puri.sm/Librem5/feedbackd) to provide vibration or audio feedback on message receive.

### Blurble [↗](https://gitlab.gnome.org/World/Blurble)

Word guessing game

[Vojtěch Perník](https://matrix.to/#/@pervoj:matrix.org) reports

> After a longer time I started working on [Blurble](https://gitlab.gnome.org/World/Blurble) again and the app has moved a few steps closer to version 1.0.0:
> 
> * Keyboard navigation has been improved. The currently active cell is highlighted and it is possible to navigate the UI using the Tab key.
> * The buttons on the keyboard are now also colored. For better playability are now keyboard buttons also colored based on if and where the character is in the word.
> * The app has been redesigned. A welcome page, help and nicer game result information have been added.
> 
> You can get the latest version from [Flathub](https://flathub.org/apps/details/app.drey.Blurble).
> 
> Also, a big thanks to the GNOME design team, namely Tobias Bernard and Allan Day, for helping come up with better solutions.
> ![](OWEMrdfYvVKunfppDdysqOoi.png)

# Shell Extensions

[oae](https://matrix.to/#/@oae:matrix.org) reports

> I have updated the Pano - Clipboard Manager with new features and fixes
> * Gnome Shell 44 support
> * Marking items as a favorite is now possible.
> * New Emoji type
> * Many customization options are added (Item styles, Pano height...).
> * Links can be now opened in the default browser
> * History can be filtered based on the item type
> * Content-aware notifications
> * Many navigation improvements
> * And many more changes. 
> * You can read more on [GitHub](https://github.com/oae/gnome-shell-pano).
> ![](PLfErcWZhypgaZWFddXMIEup.png)
> {{< video src="nejJeLBJBzXvPkroQUdhpEIU.webm" >}}
> {{< video src="odSBdObmRFOUptXAqGsBnAVr.webm" >}}

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) announces

> Extension download count is now available to the public on [extensions.gnome.org](https://extensions.gnome.org/).
> 
> Currently, [Argos extension](https://extensions.gnome.org/extension/1176/argos/) (not developed since 3.32) holding the records for more than 13.8M downloads following by [Dash to Dock extension](https://extensions.gnome.org/extension/307/dash-to-dock/) with 6.2M downloads.

# GNOME Foundation

[mwu](https://matrix.to/#/@mwu:gnome.org) reports

> My name is Melissa Wu and I am a Project Coordinator with the GNOME Foundation.  I’m in charge of special projects, fundraising and assist Caroline Henriksen with marketing and branding and Kristi Progri  with events and conferences.  I’m also the resident travel coordinator for hotels and other conference travel.  
> 
> Right now I have two focuses – Raising sponsorships for GUADEC 2023 in Riga, Lativa (https://events.gnome.org/event/101/) and organizing our volunteers.  I need help with both!! If you are interested, please feel free to reach out to me directly.  This is a great time to learn something new or share your talents with us!  
> 
> This week I am preparing for SCaLE where I will manning the booth for the next three days – fundraising, selling shirts, and sharing what's new with GNOME.  If you are in the area, come see us at the booth or stop by our GNOME Beers event on Sat (March 11) at 6pm at Craft by Smoke and Fire (30 W. Green Street. Pasadena, CA)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
