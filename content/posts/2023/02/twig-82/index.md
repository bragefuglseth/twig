---
title: "#82 Software Performance"
author: Felix
date: 2023-02-10
tags: ["gtk-rs", "gradience", "gaphor", "gnome-software", "just-perfection", "gir.core", "loupe", "glib", "denaro"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 03 to February 10.<!--more-->

# GNOME Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Georges blogged about some nice performance improvements he’s made in gnome-software recently: https://feaneron.com/2023/02/07/profiling-optimizing-gnome-software/

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> Emmanuele Bassi has just landed a new `GPathBuf` API in GLib, for easily building file paths, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3216

# GNOME Incubating Apps

### Loupe [↗](https://gitlab.gnome.org/Incubator/loupe)

A simple and modern image viewer.

[Sophie](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week we made a bunch of progress in planning out the image rendering for Loupe. Not only did we land the first [upstream](https://github.com/image-rs/image/commit/06b6052e0d531fe2dd2d30fc1f7bfd63819ccca0) tweak we needed, but we also discussed neat upcoming features in GTK that Loupe can make use of.
> 
> I also landed a few minor tweaks.
> 
> * Added a double-tap gesture on touchscreens for zooming in and out
> * Added a more natural rubber-band effect for zoom gestures when hitting the maximum or minimum zoom level
> * Added some space between the images in the sliding view
> * Fixed some issues for right-to-left text direction
> * Fixed swipe gesture on touchscreens
> * Added shortcuts for the _Home_ and _End_ button to got to the first or last image

# GNOME Circle Apps and Libraries



### gtk-rs [↗](https://gtk-rs.org/)

Safe bindings to the Rust language for fundamental libraries from the GNOME stack.

[slomo](https://matrix.to/#/@slomo:matrix.org) says

> Just in time for the GNOME 44 API/ABI freeze there was a new gtk-rs release, and new releases of the Rust bindings for various other GNOME libraries. Check https://gtk-rs.org/blog/2023/02/10/new-release.html for the release notes.

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) says

> TWIG-Bot Last weekend, [Gaphor](https://gaphor.org) 2.16.0 has been released.
> 
> * The Model browser now supports multi select
> * The GUI has been improved: diagram name shows in the header, improvements in spadding, backdrop when no diagrams are opened and icons. Thanks to Tobias Bernard.
> * The editor's CSS editor now supports dark mode and variables.
> * It's now easier to add top-level packages and diagrams in the model browser.
> ![](FsqGxZeCMgHQSeuOUZMwAFhJ.png)
> ![](rEFVFztjOOPCVcwTqZMAcsyU.png)

# Third Party Projects

[nxyz](https://matrix.to/#/@nxyz:matrix.org) says

> This week I released [Paleta](https://github.com/nate-xyz/paleta), a simple gtk4/libadwaita app that allows you to extract colors from an image and manage them in palettes. Extraction is done with the color-thief library and palettes are saved persistently. You can get it from [Flathub](https://flathub.org/apps/details/io.github.nate_xyz.Paleta).
> ![](IpLUBDZnYHvYJdhyADWuUFRh.png)

[Hunter Wittenborn](https://matrix.to/#/@hunter:hunterwittenborn.com) announces

> After a few months of work, [Celeste](https://github.com/hwittenborn/celeste) has been released. Celeste is a file synchronization application, written in GTK, that can connect to a wide variety of cloud providers, including Google Drive, Dropbox, Nextcloud, ownCloud, and WebDAV. More storage types are also planned for the future, including Microsoft OneDrive and Amazon S3.
> 
> Celeste can be installed as a [Flatpak](https://flathub.org/apps/details/com.hunterwittenborn.Celeste), [Snap](https://snapcraft.io/celeste), or [Debian package](https://github.com/hwittenborn/celeste#prebuilt-mpr-debianubuntu).
> 
> The project is currently in it's alpha state, so if you have any issues please report them on the project's [issue tracker](https://github.com/hwittenborn/celeste).
> ![](TnqjAkhmVgJpagepsFAmyMLR.png)

[fiaxh](https://matrix.to/#/@fiaxh:matrix.org) reports

> [Dino 0.4 was released](https://dino.im/blog/2023/02/dino-0.4-release/). Dino is a secure and privacy-friendly messaging application. It uses the XMPP (Jabber) protocol for decentralized communication. The release adds support for message reactions and replies. We also switched from GTK3 to GTK4 and make use of libadwaita now.
> ![](SbkzZLjRYTDDnhHvxMLrlgMo.png)

[Diego Iván](https://matrix.to/#/@dimmednerd:matrix.org) says

> This week, I released a [PDF Metadata Editor](https://github.com/Diego-Ivan/pdf-metadata-editor). Edit the title, author, keywords, creator, producer, creation and modification dates of your PDF documents. You can get it from [Flathub](https://flathub.org/apps/details/io.github.diegoivan.pdf_metadata_editor).
> ![](LTnSQvbeAAHgEvbYOGnAvcZo.png)

[Daniel Wood](https://matrix.to/#/@dubstar_04:matrix.org) announces

> This week Design, a 2D CAD application for GNOME gets all touchy!
> Adding gesture input, usability improvements on small screens and more...
> 
> * Touch input for pan, pinch to zoom and double tap to zoom all
> * Fix tabs not being shown
> * Fixes for text rotation
> * Fix files being read incorrectly
> * Fix ARC selection
> * Improve selection precision
> * Added ability to Save As
> * Handle keyboard shortcuts
> 
> Design 43-alpha3 is available from Flathub:
> https://flathub.org/apps/details/io.github.dubstar_04.design
> ![](sDMKQOjlTISsGlTIiuyMcSYP.png)

### Gradience [↗](https://github.com/GradienceTeam/Gradience)

Change the look of Adwaita, with ease.

[tfuxu](https://matrix.to/#/@tfuxu:matrix.org) says

> This week, Gradience version 0.4.0 have been released, this release is a preparation for 0.8.0, the next major release that will bring GNOME Shell theming support and other long awaited features. Version 0.4.0 introduces a CLI interface, some bug fixes and general quality of life improvement changes.
> 
> Here are some of the changes:
> 
> * Added CLI interface, useful for creating scripts or for those who prefer terminal tools (CLI guide available [here](https://github.com/GradienceTeam/Gradience/wiki/Using-CLI))
> * Now Gradience warns user when switching to other presets, if current one has unsaved changes
> * When running Gradience from terminal, you will see easier to understand error messages, thanks to the new logging facility
> * Fixed sorting in "Explore" tab of Preset Manager not working with non-English locales
> * Now Gradience will now internally use hexadecimal color values or RGBA formatted colors if transparency is used
> 
> And some other bug fixes and under-the-hood improvements. You can find more detailed changelog [here](https://github.com/GradienceTeam/Gradience/releases/tag/0.4.0).
> 
> As always, the latest release is available to download from [Flathub](https://flathub.org/apps/details/com.github.GradienceTeam.Gradience).
> ![](gQBbrZbJCtoOwvYNlwBJwopY.png)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) announces

> Gir.Core 0.3.0 was released. Most prominent features in this release include:
> * Addition of WebKitGtk to the supported libraries
> * Improved runtime behaviour if using .NET 7 instead of .NET 6
> * New C test library to not rely on API of other libraries to do unit tests
> * Support for [detailed signals](https://docs.gtk.org/gobject/concepts.html#the-detail-argument) via `GObject.Signal.Connect`
> * The `GType` of a class / interface is now available in the public API
> 
> Additionally there were several bugs fixed, some new API was made available and some code cleanup took place. For details checkout the [release notes](https://github.com/gircore/gir.core/releases/tag/0.3.0).

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Denaro [V2023.2.0](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.2.0) is here! Your favorite weekly pizza man is back with a new update for your favorite personal finance manager! In this update, we gave users the ability to add passwords to protect and encrypt their account files, we provided a way for users to transfer money between accounts of different currencies, and we improved the look and feel of the application, as well as many, many other features and enhancements. 
> Here's a full changelog:
> * Added the ability to add a password to an account (This will encrypt the nmoney file)
> * Added the ability to transfer money between accounts with different currencies by providing a conversion rate in TransferDialog
> * Added the ability to configure how Denaro uses locale separators in amount fields
> * Added the ability to copy individual transactions
> * Added the ability to sort transactions by amount
> * LC_MONETARY and LC_TIME will now be respected
> * Added "Help" button to toast notification when there are 0 imported transactions 
> * Recent accounts are now available to select from the TransferDialog
> * Improved importing of qif and ofx files
> * Added a "New Window" action to the main menu
> ![](egSswjfozVpaAbidbfZVbBYk.png)
> ![](ihYhZRDlvjlnxVoOwPjwuOKg.png)
> ![](fqwqNdiobtyQDudjCUfqyLpj.png)

# Shell Extensions

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) reports

> Version 16 of Auto Activities has been released.
> 
> In this new version, the user is able to choose between showing the activities overview or the apps grid when there are no windows in the workspace. In addition, the maximum window check delay limit has been increased from 1 to 10 seconds
> 
> Special thanks to [Zetta1Reid0](https://github.com/Zetta1Reid0) who helped implement this.
> 
> [Follow the development](https://github.com/CleoMenezesJr/auto-activities)
> [Get it on GNOME Extensions](https://extensions.gnome.org/extension/5500/auto-activities/)
> ![](WqeGFkOFXSKpFkhijxjyOKya.png)
> {{< video src="IZsZtHeNRbctFwMpFmZDEzSC.webm" >}}

### Just Perfection [↗](https://extensions.gnome.org/extension/3843/just-perfection/)

A tweak tool to customize the GNOME Shell and to disable UI elements.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) says

> [Just Perfection GNOME Shell Extension](https://extensions.gnome.org/extension/3843/just-perfection/) version 23 released with more features such as overview spacing size, screen recording and sharing indicator visibility, switcher popup delay, ...
> 
> This version named after Spanish painter Francisco Goya.
> 
> We also had some [bug fixes](https://gitlab.gnome.org/jrahmatzadeh/just-perfection/-/blob/master/CHANGELOG.md) in this version.
> 
> {{< youtube VyY5MXKeqgk >}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

