---
title: "#83 Sharing Is Caring"
author: Felix
date: 2023-02-17
tags: ["gnome-contacts", "gnome-shell", "kgx", "tracker", "gnome-weather", "cawbird", "libadwaita", "epiphany", "gnome-control-center", "gtk", "tubeconverter"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 10 to February 17.<!--more-->

# GNOME Core Apps and Libraries

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) announces

> Thanks to the contribution of Hendrik Müller, the Wi-Fi panel is now able to generate QR codes to share the connection.
> ![](0526659d409030c502c0105c9bd8b751c02d84f5.png)

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) reports

> Thanks to the outstanding work of Felipe Borges and Jakub Steiner, the Mouse & Touchpad panel has received a gorgeous facelift, with animations exemplifying what the preferences of the panel do. It also received a redesigned behavior test dialog, and a new "Pointer Assistance" preference that controls the mouse acceleration profile.
> {{< video src="b7a233bb8eaf5e6ee31131f5b685d1a89b5f9967.webm" >}}

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[verdre](https://matrix.to/#/@verdre:gnome.org) announces

> The GNOME Shell team has been busy finishing up some major changes to quick settings this week: Thanks to @fmuellner and Georges Stavracas (feaneron) GNOME Shell quick settings [now have subtitles](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2619), the bluetooth toggle [got a submenu](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2501) to allow for quickly connecting to devices, and we [show a list of apps](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2624) that are running in the background.
> ![](b7259df7a4771c7e130e9fe0a175cf2a0b1931f2.png)
> ![](6f1340420e4d5ad0793e15768f1b3a5933d82dbb.png)
> ![](ce67520023b0922bfbe7c6e6fd86330a54cf4244.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> since GTK has recently [added](https://docs.gtk.org/gtk4/method.Snapshot.append_scaled_texture.html) support for better texture filtering, `AdwAvatar` now looks better with scaled images when using `GdkTexture` as a custom paintable. Apps using custom paintables will have to update their code separately to get the same result.
> ![](329a38e5b7a43e5a5e2495ba6cb718c7ce949ed2.png)

### GNOME Console [↗](https://gitlab.gnome.org/GNOME/console)

A simple user-friendly terminal emulator.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> Console now has a tab overview, replacing the old tab switcher on small sizes but also available on desktop in addition to tab bar
> ![](8cde9bcc1628e79b7f9663106bad7c168647fbab.png)
> ![](bab07f8598ccb176f80926a0ca5da850c523123d.png)

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Thanks to Jordan Petridis GTK Demo and Widget Factory are now available for aarch64 in the GNOME nightly flatpak repository. https://wiki.gnome.org/Apps/Nightly
> ![](4007ed449c4b58914e79f7579c12de1149d83921.png)

### Weather [↗](https://gitlab.gnome.org/GNOME/gnome-weather)

Show weather conditions and forecasts.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Weather has received some last-minute UI updates for GNOME 44:
> 
> * The details view now has a flat headerbar.
> * The temperature line is now a smooth curve.
> 
> Thanks to Cleo Menezes Jr. and Oscar Pritchard, respectively, for their MRs!
> ![](72c44d3b1dcb9461fb6e67aa36333ff68cbb4d8c.png)

### GNOME Contacts [↗](https://gitlab.gnome.org/GNOME/gnome-contacts)

Keep and organize your contacts information.

[nielsdg](https://matrix.to/#/@nielsdg:gnome.org) announces

> GNOME Contacts 44.beta has been released! This is an exciting new release with some cool new stuff:
> * Contacts can now be shared with a QR code thanks to Hendrik Müller, which allows for a simple and easy way of passing on contact information across devices
> * Birthdays on a leap day will now get a birthday reminder on February 28 in non-leap years thanks to Ruben Schmidmeister
> * The main menu has been renamed to follow the GNOME HIG (kudos to Hari Rana (TheEvilSkeleton)!) and now contains an action item to export all contacts
> * Added support for multiple keyboard shortcut that were previously missing, like  `<Ctrl>F`, `<Ctrl>,`, `<Ctrl>Enter` and more
> * It was previously impossible to remove a birthday from a contact, which has now been fixed
> * Some translation issues were found and solved with the help of Sabri Ünal
> * A bug where some title buttons where shown twice when using left-aligned title buttons has been fixed with the help of Markus Göllnitz

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[Jamie](https://matrix.to/#/@itsjamie9494:gnome.org) announces

> Epiphany has a brand new UI for handling permissions, to replace the ageing GtkInfoBar solution
> ![](8bd9d5064d1c4aee942137614ee9f0b9eb04d766.png)
> ![](7b10bfda75bf2eb587c19de9e30df735fc81e0ff.png)

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) announces

> Commandline tagging is back - the Tracker 3.5.0beta release fixes some issues in the `tracker3 tag` command, and adds test coverage to guard against future regressions.

# Third Party Projects

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> verdre made and released a medication tracker app named "Capsule" https://flathub.org/apps/details/nl.v0yd.Capsule
> ![](56785a5434fbadca620895a37b69c791d0e37484.png)
> ![](84ef05056579692d069200edb6360670c3695061.png)
> ![](d4733388a00f03c9bd9eb4331c1b8c7391300c37.png)

[gianniradix](https://matrix.to/#/@computerbustr:matrix.org) says

> Aviator 0.2.0 has dropped! This update includes a newer version of SVT-AV1 with defaults that more effectively tune for visual quality & support for film grain synthesis applied as a filter at decode time, a progress bar, a Stop Encode button, support for more buttons in the header bar, a stereo downmixing switch for audio, AdwAboutWindow, and more! You can read more about Aviator [here](https://github.com/gianni-rosato/aviator#readme)
> ![](nzBGahDcewHzOYcLRCeYpYJY.png)

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> I released a new version of Oh My SVG featuring the new icon by Sam Hewitt and several design and quality of life improvements. Get it on Flathub: https://beta.flathub.org/apps/re.sonny.OhMySVG
> ![](e8b19d56a39d45d6f8632b2af2a010fdbd95714b.png)

[Aaron Erhardt](https://matrix.to/#/@aaron:matrix.aaron-erhardt.de) says

> [Relm4](https://github.com/Relm4/Relm4) 0.5, an idiomatic GUI library based on gtk4-rs, was just released with many improvements.
> 
> Since the first beta release, we have extended the support for asynchronous code, added widget templates, more tooling and a lot of other improvements. You can find more information about the release in the [official blog post](https://relm4.org/blog/posts/announcing_relm4_v0.5).

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Tube Converter is in the process of being rewritten in C#. The rewrite brings a more stable backend to Tube Converter with support for download progress and better preformance when running multiple downloads. We hope to have our first beta released on Sunday, once a WinUI version is complete. [Check out the progress here](https://github.com/nlogozzo/NickvisionTubeConverter/pull/50) !
> ![](bwCQbSKJFIqVipOoIELFaebv.png)

### Cawbird [↗](https://ibboard.co.uk/cawbird/)

A native Twitter client for your Linux desktop.

[CodedOre](https://matrix.to/#/@coded_ore:matrix.org) reports

> Hello everyone! It's been a while since the last update on our GTK4 rewrite of Cawbird.
> 
> Sadly the biggest news is not the recently completed rework of the session system, which should handle interaction better. Instead it is that Twitter has banned all third-party clients. As a result, we have dropped the backend for the Twitter 2.0 API. Instead being a multi-platform client, the focus is now on Mastodon alone.
> 
> Anyway, apart from the rework done for sessions, there were a few additions lately:
> 
> * The user profile page now shows when a link was verified by Mastodons servers.
> * Thanks to IBBoard, posts can now be liked and reposted in the client.
> * Initial support for hiding sensitive content.
> 
> I know there weren't news about this rewrite in the last months, but I hope I can share more news soon!
> ![](XovEkKSVGbKZTXkqblgpyskf.png)
> ![](rvMnYdsoFxLJdkVdLAynQbeh.png)
> ![](PbVyvpHGBQzEPyUIpQHEqMmh.png)

# Miscellaneous

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> Linux App Summit call for proposal closes tomorrow (18/02) - last chance to submit a talk, workshop or bof proposal. https://linuxappsummit.org/cfp/

[Philip Goto](https://matrix.to/#/@flipflop97:matrix.org) reports

> Many GNOME projects make use of the internal translation system Damned Lies, but there are also a lot of projects that don't, including GNOME Circle apps. Because these projects can be easily looked over, I made an overview of them with links to their translation systems.
> 
> https://gitlab.gnome.org/philip.goto/external-translations

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) announces

> The GNOME Foundation staff wants to increase communications with the wider GNOME community. The plan is for us to take turns writing updates here to let y'all know who we are and what we are doing. 
> 
> Greetings! I am the Director of Operations for the GNOME Foundation. After spending last week at FOSDEM and State of Open Con, this has been a catch up week for me: going through the mounds of paper on my desk, working on GNOME's financial records, prepping invoices for the advisory board, handling tax forms, paying bills, and other not-exactly-exciting-but-essential-stuff.
> 
> Volunteer opportunity:
> GNOME will be hosting a booth at SCaLE in Pasadena March 10-12. If you are in the Los Angeles area, we could use your help with staffing our booth. Contact info@gnome.org or zana on matrix to help.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

