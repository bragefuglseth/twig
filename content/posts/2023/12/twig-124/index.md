---
title: "#124 Fixes and Improvements"
author: Felix
date: 2023-12-01
tags: ["gaphor", "glib", "kooha", "pods"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 24 to December 01.<!--more-->

# Sovereign Tech Fund

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) announces

> As part of our infrastructure [initiative funded by the Sovereign Tech Fund](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/), a number of community members have been hard at work for the past weeks.
> 
> Some highlights of what landed this week:
> 
> * Julian fixed a scrolling bug in GNOME Shell https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3023
> * Sam landed some more High Contrast stylesheet fixes in GNOME Shell https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3030
> * Sonny added builtin WebP support to GNOME Platform/SDK https://floss.social/@sonny/111505420648908207
> * Philip finished the work to drop gtk-doc support from GLib https://gitlab.gnome.org/GNOME/glib/-/issues/3037
> * Adrian reworked systemd-homed internals to allow an admin user to change a standard user's account settings without the user's password
> * Hubert has been hunting memory leaks in Flatpak and portals related project
> 
> New projects started this week:
> 
> * Tobias is working on overhauling the design for "Open With" dialogs, adding support for apps to register themselves as URL handlers for a specific domain https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/open-with/open-with.png
> * Evan Welsh joined the team to help on improving language bindings
> * Matt Campbell joined the team to work on the prototype for a new a11y architecture https://blogs.gnome.org/a11y/2023/10/27/a-new-accessibility-architecture-for-modern-free-desktops
> * We are working on a strategy for improving the experience of users with visual impairement
> * The team is collaborating with systemd for TPM backed secrets encryption and storage for the desktop keyring

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> gtk-doc has been removed from GLib and all the documentation is now generated using gi-docgen. Help is needed to update the syntax in doc comments to re-enable links in the documentation! See https://gitlab.gnome.org/GNOME/glib/-/issues/3037

# GNOME Circle Apps and Libraries

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Dan Yeaw](https://matrix.to/#/@Yeaw:matrix.org) says

> Gaphor, the simple UML and SysML modeling tool, version 2.22.0 is now out! Some of the major changes include:
> 
> - Add app preferences for overriding dark mode and the language to English. As part of this we helped improve libadwaita 1.4.0 support on macOS and Windows.
> - Proxy port improvements
> - Add allocations toolbox with allocate relationship item
> - Add members in model browser
> - Make line selection easier by increasing tolerance
> - Make model loading more lenient
> 
> The new version is available on Flathub.
> ![](cVmqMkKjEcZDXrlZGiAqZOGV.png)

# Third Party Projects

[دانیال بهزادی](https://matrix.to/#/@danialbehzadi:mozilla.org) says

> Carburetor 4.2.0 released with a new icons and it's now on [Flathub](https://flathub.org/apps/io.frama.tractor.carburetor) too. Carburetor is built upon Libadwaita to let you easily set up a TOR proxy on your session, without getting your hands dirty with system configs. Initially aimed at simplifying life for GNOME enthusiast on their mobiles, it's now fully usable with mouse and/or keyboard too.
> ![](9904ddfb00bea4fb81fb9a13d874c64ae66400df1729607475272876032.png)

### Pods [↗](https://github.com/marhkb/pods)

Keep track of your podman containers.

[Marcus Behrendt](https://matrix.to/#/@marhkb:matrix.org) reports

> I have released [Pods](https://flathub.org/apps/com.github.marhkb.Pods) in version 2.0.0. The biggest changes are that volumes are now supported and that the basic layout of the interface has been redesigned. Pods now has a completely new look, with a sidebar and new libadwaita 1.4 widgets.
> ![](nncHFtaXLjbpzGxpmMweMMCe.png)

### Kooha [↗](https://github.com/SeaDve/Kooha)

Elegantly record your screen.

[Dave Patrick](https://matrix.to/#/@sedve:matrix.org) says

> Aside from a range of critical fixes, [Kooha](https://github.com/SeaDve/Kooha) is getting a new UI to address previous design limitations and enhance the overall experience.
> ![](oIHeInWvLflVbjbvXnbvniRd.png)

# Documentation

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) announces

> A new release of gi-docgen, [2023.2](https://gitlab.gnome.org/GNOME/gi-docgen/-/releases/2023.2), the documentation generator for C libraries using gobject-introspection. Lots of quality of life improvements, as well as larger changes, like:
> 
> * parse the default value attribute for GObject properties
> * a complete redesign of the search results, with better output on smaller form factors
> * support for admonitions (note, warning, important) inside the documentation blocks
> * display the implemented interfaces in the class pseudocode description
> * add a link in extra content files to their source in the code repository
> * and much more…
> 
> You can download gi-docgen 2023.2 from [the GNOME file server](https://download.gnome.org/sources/gi-docgen/2023/gi-docgen-2023.2.tar.xz) or from [PyPI](https://pypi.org/project/gi-docgen/).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

