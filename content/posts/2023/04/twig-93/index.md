---
title: "#93 Snapshot"
author: Felix
date: 2023-04-28
tags: ["flatseal", "glib", "libadwaita", "amberol"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 21 to April 28.<!--more-->

# GNOME Incubating Apps

[Maximiliano 🥑](https://matrix.to/#/@msandova:gnome.org) says

> Snapshot was recently accepted into the [Incubator](https://gitlab.gnome.org/Incubator/) group and the first new preview release is out. Snapshot aims to be the next generation camera app for GNOME, supporting both desktop and mobile devices.
> 
> You can get Snapshot at [Flathub](https://flathub.org/apps/org.gnome.Snapshot).
> ![](e9b24c02d04a7cce22e66e0d5fd04dd0cafb3a1d.png)

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/they)](https://matrix.to/#/@alexm:gnome.org) announces

> after almost a year, [breakpoints](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.BreakpointBin.html) have finally landed in libadwaita. They allow to do arbitrary layout changes depending on the bin/window size and aspect ratio, with the tradeoff of losing automatic minimum size calculation. This finally allows to do things such as adding a bottom bar on narrow sizes without issues, and enable a lot of designs that are currently impractically hard to implement.
> 
> Breakpoints can also be used directly on [`AdwWindow` and `AdwApplicationWindow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Window.html#breakpoints) for convenience.
> {{< video src="b0801f360fe501abc4497c50c08030ec84ff3afd.mp4" >}}

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) says

> GLib has just acquired an internal list of pending `GTask`s, for debugging what’s going on in your app using gdb. Use it by calling `print g_task_print_alive_tasks()` in gdb. See https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3404

# GNOME Circle Apps and Libraries

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) says

> This week, [Telegraph](https://apps.gnome.org/app/io.github.fkinoshita.Telegraph/) joined GNOME Circle. With Telegraph, you can translate Morse code back and forth. Congratulations!
> ![](95ee03f452bcd44862ce26291053b35fef0ac729.png)

### Amberol [↗](https://gitlab.gnome.org/ebassi/amberol/)

Plays music, and nothing else.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> You thought I forgot about Amberol, but here we are, with a new release! And what a release is this, with lots and lots of fixes big and small:
> 
> * you can now restore the playlist from your last session
> * background playback can be toggled
> * there's a quick mute/unmute button
> * no duplicate songs in the playlist
> * the UI has been slightly tweaked to avoid confusing the volume scale with the song position
> * the readability of the drop overlay has been improved
> * the base run time and dependencies have been updated to GNOME 44
> * lots and lots of big and small fixes
> 
> Plus, Amberol is now verified on the new [Flathub](https://flathub.org/apps/io.bassi.Amberol) website, and we're close to 100k downloads in a bit over a year of development!
> ![](e6b512c56e701a8c600dd0035994af853447c342.png)
> ![](8b49bcd21a8aeccebe002a515c648e63e86e5058.png)
> ![](a7027679450bd7b102d141a1d603b84bd62b67d7.png)

# Third Party Projects



### Flatseal [↗](https://github.com/tchx84/Flatseal)

A graphical utility to review and modify permissions of Flatpak applications.

[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) says

> To celebrate Flatseal reaching [800,000](https://flathub.org/apps/com.github.tchx84.Flatseal) downloads on Flathub 🤯, a new release is out! [Flatseal 2.0](https://blogs.gnome.org/tchx84/2023/04/28/flatseal-2-0/) comes with improved visuals powered by GTK 4 and Libadwaita and, with that, a few quality of life improvements and bug fixes.
> 
> Kudos to [natasria](https://github.com/natasria) for the initial work on porting the user interface to GTK 4 and Libadwaita, and to [A6GibKm](https://github.com/A6GibKm) for helping with the reviews and making **a lot** of further improvements to that work.
> 
> Download on [Flathub](https://flathub.org/apps/com.github.tchx84.Flatseal)!
> ![](rkwfAPnzmlOgWgGNYdPjfAoU.png)

# Miscellaneous

[Casper Meijn](https://matrix.to/#/@caspermeijn:matrix.org) announces

> A long-standing issue with the GNOME CI templates is fixed. Rust apps were built three times by the template. The old template executes a normal build. Then rebuilds all the dependencies in preparation for the tests. And will accidentally build again during the test execution. With the recent change, the CI job rewrites the Flatpak manifest to enable `run-tests`. Flatpak will take care of executing the test suite during the build. 
> 
> https://gitlab.gnome.org/GNOME/citemplates/-/issues/5

# GNOME Foundation

[Rosanna](https://matrix.to/#/@zana:gnome.org) says

> (Some of the hats I wear here at the Foundation include work on the Travel Committee, the Code of Conduct Committee, the Executive Committee, the Finance Committee, as well as the staff liaison to the Board of Directors. For the sake of clarity, I will avoid using the word "we" without specifying which group I am referring to.)
> 
> With another successful LAS conference completed, the Foundation staff spent this week recovering, traveling, or switching gears. With scheduling for GUADEC happening this week, the travel committee is also getting into gear to go through travel requests. I have also been invoicing the generous folks who have committed to sponsoring GUADEC this year. 
> 
> The Code of Conduct Committee has been made aware that the sending of reports to discourse is not working for some people. The committee and staff are working on an alternative at blogs.gnome.org/coc/. Reports can already be sent via this webpage, but it does need a bit more work. It needs to allow anonymous reports and to link to the code of conduct. Only those on the Code of Conduct Committee and moderators on gitlab.gnome.org have access to these reports. If you wish to report on anyone on that list, you can still personally email a report to the other members of the committee.
> 
> I have had some meetings with some nonprofit bookkeepers in an effort to find help to lighten my load. I am looking forward to carving out more time to be able to catch up on some of my other tasks.
> 
> In other goings on this week, I have also corresponded with the 401K accountant to figure out some discrepancies for a tax filing, updated the employee handbook, paid some bills, filled out and uploaded some forms for compliance, and spent time at the bank. Just another regular week working for the Foundation.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

