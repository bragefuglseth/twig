---
title: "#120 Updated Documentation"
author: Felix
date: 2023-11-03
tags: ["phosh", "hebbot", "tagger", "blueprint-compiler", "glib"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 27 to November 03.<!--more-->

# GNOME Core Apps and Libraries

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> GLib has seen recent improvements for support on Hurd and musl, and is in the middle of an exciting port from gtk-doc to gi-docgen!

### Evolution Data Server

[Corentin Noël](https://matrix.to/#/@tintou:matrix.org) announces

> This week the Evolution Data Server library gained [a new homepage for its documentation](https://gnome.pages.gitlab.gnome.org/evolution-data-server/) using GitLab for an always up-to-date version and GI-Docgen to align with the documentation of other libraries in the ecosystem.

### VTE / Terminal

[hergertme](https://matrix.to/#/@hergertme:gnome.org) announces

> The venerable GNOME Terminal is working towards moving to GTK 4! You can find an initial implementation has landed in the git repository. A number of things need to be worked out for GNOME 46 and you can help make that happen! See https://gitlab.gnome.org/GNOME/gnome-terminal/-/wikis/Gtk4Warts for some outstanding issues.
> 
> Additionally, VTE will now time it's rendering work based on the GdkFrameClock. This removes a longstanding issue where rendering would be capped around 40 frames-per-second.
> ![](b5d950b59625ec14094da726b4880d87242522d41719130765574275072.png)

# Third Party Projects

[Casper Meijn](https://matrix.to/#/@caspermeijn:matrix.org) says

> This week I [released](https://www.caspermeijn.nl/posts/read-it-later-0.5.0/) version 0.5.0 of Read It Later. This is a client for [Wallabag](https://www.wallabag.it/en), which allows you to save web articles and read them later. The significant changes are the upgrade to GNOME 45, a crash fix, better support for links in articles, and new translations. Download on [Flathub](https://flathub.org/apps/details/com.belmoussaoui.ReadItLater).

[paddis 🌻🐢](https://matrix.to/#/@turtle:turtle.garden) says

> I released the first version of Jellybean, an app that allows you to manage inventories of various items, this week!
> You can download it from Flathub [here](https://flathub.org/apps/garden.turtle.Jellybean).
> ![](oeHJaRaMttyBF1iaFGfbrmWPJSiCgt8R.png)

[kaii](https://matrix.to/#/@kaii-lb:matrix.org) reports

> [Overskride](https://github.com/kaii-lb/overskride) v0.5.2 was released!
> 
> this release brings a lot of new features:
> * Audio profile support! now you can choose what profile to use with supported devices
> * Battery polling, so you can see the battery of the connected device
> * Auto accept files from trusted devices
> * ...and a lot more fixes and quality of life improvements!
> ![](pVQHLtEzZTBhOiUxvPjAfwQs.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

Tag your music.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Tagger [V2023.11.0](https://github.com/NickvisionApps/Tagger/releases/tag/2023.11.0) is here!
> 
> This release simply fixes an issue where Tagger crashed when loading album art data for some files.
> ![](hLpGIauGhjuGCEHLCdCEuXIG.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) announces

> Hot of the press is [Phosh](https://gitlab.gnome.org/World/Phosh) 0.33.0. The
> release is a bit smaller than usual as we spent some timer modernizing
> [phoc](https://gitlab.gnome.org/World/Phosh/phoc) to work well with wlroots git
> but there's still some user visible improvements: 
> 
> * a consistent toggle for password visibility in modal dialogs
> * a new lockscreen plugin to spawn apps. Most useful when you forgot to launch
>   your music player and don't want to unlock just for that
> * better troubleshooting information
> * improved support for Indic languages
> 
> Check the full details [here](https://phosh.mobi/releases/rel-0.33.0/)
> ![](VsZHjAwfnmxPIiEHBeVjElfa.png)

### Hebbot [↗](https://github.com/haecker-felix/hebbot)

Hebbot is the bot behind TWIG that manages all the news.

[Felix](https://matrix.to/#/@felix:haecker.io) says

> This week there were some technical difficulties, because TWIG-Bot (Hebbot) crashed during the creation of the issue (which is the reason for the delay). 
> 
> This was caused by a [rare bug involving Unicode text splitting](https://github.com/haecker-felix/hebbot/commit/e1dadab9b9438ef3bd57e4c3164d3c6e01d78928). Thanks to the help of Sophie and Zander Brown, the problem was quickly identified and eliminated. I deployed a new updated version of the bot which fixes the crash.
> 
> Happy ending, the weekend is saved, I can publish TWIG! 🥳

### Blueprint [↗](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/)

A markup language for app developers to create GTK user interfaces.

[gregorni](https://matrix.to/#/@gregorni:gnome.org) announces

> This week, the Blueprint formatter was merged! 🎉 You no longer have to worry about redundant whitespaces, forgotten indentations or random newlines anymore when editing your .blp files! This will make writing UI templates for GTK apps a _lot_ more pleasant for developers.
> 
> The formatter is also part of the Blueprint LSP, meaning it should be integrated into development tools like GNOME Builder, VS Code and Workbench pretty soon. 
> 
> I'd like to thank James Westman and Sonny for guiding me through the process of writing the formatter, they helped me a lot.
> {{< video src="e71b221fd9d0d8cfeeee36d18fa4cc17d6dd12691720434448710762496.webm" >}}
> {{< video src="058c761cb10c69b8ae4f89349a88b74808d1d71e1720436042793746432.webm" >}}

# GNOME Websites

[Sophie (she/her)](https://matrix.to/#/@sophieherold:gnome.org) reports

> After [pitching a _Welcome to GNOME_ website](https://discourse.gnome.org/t/welcome-to-gnome-a-landing-page-for-newcomers/14668) in March of this year, the pages are now [ready for review](https://teams.pages.gitlab.gnome.org/Websites/welcome.gnome.org/).
> 
> [_Welcome to GNOME_](https://teams.pages.gitlab.gnome.org/Websites/welcome.gnome.org/) not only gives a lot of information about how to get started in GNOME but also generates a page for each Core, Circle, and Development app, providing the correct links and instructions for the respective app. That includes docs for the right programming language and GTK version, instructions on installing nightly builds if available, instructions on setting up GitLab or GitHub respectively, and much more customized information.
> 
> You can use the [project’s issue tracker](https://gitlab.gnome.org/Teams/Websites/welcome.gnome.org/-/issues) for feedback or create pull requests with content additions or corrections. If the content seems stable enough the pages will be submitted to our translation system within the next weeks, allowing for translation before the release.
> ![](f4086e5a22e2a57e3471976ab8a8b840a0e55ed61720507194933248000.png)

# Shell Extensions

[somepaulo](https://matrix.to/#/@somepaulo:matrix.org) announces

> A new version of the [Weather or Not](https://extensions.gnome.org/extension/5660/weather-or-not/) extension has been released, introducing a control to change the position of the indicator in the panel. You can now choose to place the indicator in either the left or right panel boxes or in one of four different positions in the central box. This is implemented in the main, GS45+ branch of the extension only.
> ![](ZycsNOuRchErBqEUHRLhNSLI.png)

# Miscellaneous

[barthalion](https://matrix.to/#/@barthalion:matrix.org) says

> [flatpak-builder-lint](https://github.com/flathub/flatpak-builder-lint) is now capable of checking build directories and ostree repositories, regardless of the build tool used to produce an app. There also are new checks that ensure screenshots are properly mirrored and AppStream passes appstream-glib's validation. We have also started publishing a Docker image to make it easier to integrate it with external CI systems. See its README for details!
> 
> We are putting the finishing touches to the updates moderation in Flathub. We have been historically very lenient past the initial review, trusting no one will try to sneak through with potentially shoddy AppStream changes. New builds changing certain user-facing fields will be marked for review and withheld until manually approved (or not!).
> 
> The server-side validation and the moderation dashboard are the backbone of the upcoming direct uploads feature, that will allow verified developers to publish their apps directly, without poking me or being forced to use either GitHub or flatpak-builder. There is no launch date for that yet, but we are getting there!
> ![](SOWHtyUFCwtlOpwMYXneFeZW.png)

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) reports

> Thanks to [the stellar work of Arjan Molenaar](https://gitlab.gnome.org/GNOME/pygobject/-/merge_requests/250), the pygobject bindings have been dramatically improving over the past month. Now Python developers can finally use instances of fundamental types, which was one of the big blockers for people implementing custom widgets with GTK4.
> 
> Starting with the GNOME 46 run time, you'll be able to do advanced custom drawing using render nodes, as well as accessing low level windowing system event objects, in your Python applications.

# GNOME Foundation

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) reports

> Applications have now closed for the December Outreachy project, "Implement end-to-end tests for GNOME OS using openQA". We received a number of very promising contributions to the openqa-tests and applications for the internship. Thanks to everyone who took the time to get involved. The accepted interns will be announced on **Nov. 20, 2023 at 4pm UTC**.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

